//
//  AppDelegate.h
//  real-v2-ios
//
//  Created by Derek Cheung on 16/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RETabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RETabBarController *tabBarController;


+ (AppDelegate *)sharedDelegate;

@end

