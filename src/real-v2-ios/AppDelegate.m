//
//  AppDelegate.m
//  real-v2-ios
//
//  Created by Derek Cheung on 16/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AppDelegate.h"
#import "REDatabase.h"
#import "REScheduler.h"
#import <AFNetworking/AFNetworking.h>
#import "RESearchBarViewController.h"
#import "RESwipeViewController.h"
#import "RECardInfoViewController.h"
#import "REAgentListingViewController.h"

#import <GoogleMaps/GoogleMaps.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - Override

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor blackColor];
    [REDatabase sharedInstance];
    
    REAgentListingViewController *left = [REAgentListingViewController demoViewController];
    RESearchBarViewController *center = [RESearchBarViewController demoViewController];
    RECardInfoViewController *right = [RECardInfoViewController demoViewController];
    center.leftViewcontroller = left;
    center.Rightpageviewcontroller = right;
    RESwipeViewController *testVC = [RESwipeViewController swipeViewControllerWithLeftViewController:left centerViewController:center rightViewController:right];
    center.mmDrawerController = testVC;
    testVC.touchDelegate = center;
    
    self.window.rootViewController = testVC;
    [self.window makeKeyAndVisible];
    
    [GMSServices provideAPIKey:kGoogleAPIKey];
    // Logging
    [self setupLoggings];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

+ (AppDelegate *)sharedDelegate {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

#pragma mark - Helpers

- (void)setupLoggings {
    
    setenv("XcodeColors", "YES", 0);
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:ddLogLevel];
    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:ddLogLevel];
    [[DDTTYLogger sharedInstance]setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:242 / 255.0 green:58 / 255.0 blue:67 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_ERROR];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:58 / 255.0 green:169 / 255.0 blue:242 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_DB];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:242 / 255.0 green:113 / 255.0 blue:58 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_API];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:172 / 255.0 green:242 / 255.0 blue:58 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_INFO];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor lightGrayColor] backgroundColor:nil forFlag:LOG_FLAG_DEBUG];
    
    DDLogDebug(@"Debug mode = %@", kDebugMode ? @"YES" : @"NO");
}

@end
