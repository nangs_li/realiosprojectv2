//
//  REDatabase.h
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

@class REDBObject, REAPIQueue, REDBAPIQueue, REGooglePlaceObject, REAgentListing, REAgentProfile;

@interface REDatabase : NSObject

#pragma mark - General

+ (REDatabase *)sharedInstance;

- (void)deleteAllDBData;

#pragma mark - Address Search

- (void)createOrUpdateGooglePlaceObject:(REGooglePlaceObject *)placeObject;

- (NSArray *)getLatestPlaceObject;

#pragma mark - APIQueue

- (void)createOrUpdateAPIQueue:(REAPIQueue *)apiQueue;

- (NSMutableArray *)getPendingAPIQueues;

- (void)updatePendingQueuesWithAPIQueue:(REAPIQueue *)apiQueue;

#pragma mark - AgentProfile

- (void)createOrUpdateAgentProfile:(REAgentProfile *)agentProfile;

- (NSMutableArray<REAgentProfile *> *)getAgentProfileFromMIDArray:(NSMutableArray<NSString *> *)mIDArray;

#pragma mark - AgentListing

- (void)createOrUpdateAgentListing:(REAgentListing *)agentListing;

- (NSMutableArray<REAgentListing *> *)getAgentListingFromLIDArray:(NSMutableArray<NSString *> *)lIDArray;

@end
