//
//  REDBGooglePlaceObject.h
//  real-v2-ios
//
//  Created by Alex Hung on 19/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REDBObject.h"

@class REGooglePlaceObject;

@interface REDBGooglePlaceObject : REDBObject

@property (nonatomic, strong) NSString *placeId;
@property (nonatomic, strong) NSString *name;


- (REGooglePlaceObject *)toREObject;

@end
