//
//  REDBGooglePlaceObject.m
//  real-v2-ios
//
//  Created by Alex Hung on 19/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REDBGooglePlaceObject.h"
#import "REGooglePlaceObject.h"

@implementation REDBGooglePlaceObject

+ (NSString *)primaryKey {
    return @"placeId";
}

+ (NSArray *)indexedProperties {
    return @[@"placeId"];
}

- (REGooglePlaceObject *)toREObject {
    REGooglePlaceObject *place = [[REGooglePlaceObject alloc]init];
    place.name = self.name;
    place.placeId = self.placeId;
    
    return place;
}

@end
