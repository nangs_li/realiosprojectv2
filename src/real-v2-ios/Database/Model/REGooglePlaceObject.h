//
//  REGooglePlaceObject.h
//  real-v2-ios
//
//  Created by Alex Hung on 19/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REObject.h"

@class REDBGooglePlaceObject, GMSAutocompletePrediction;

@interface REGooglePlaceObject : REObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *placeId;

- (REDBGooglePlaceObject *)toREDBObject;

- (id)initWithGMSAutocompletePrediction:(GMSAutocompletePrediction *)prediction;

@end
