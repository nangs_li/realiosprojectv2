//
//  REGooglePlaceObject.m
//  real-v2-ios
//
//  Created by Alex Hung on 19/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REGooglePlaceObject.h"
#import "REDBGooglePlaceObject.h"
#import <GoogleMaps/GMSAutocompletePrediction.h>

@implementation REGooglePlaceObject

- (REDBGooglePlaceObject *)toREDBObject {
    REDBGooglePlaceObject *dbGooglePlaceObejct = [[REDBGooglePlaceObject alloc] init];
    dbGooglePlaceObejct.placeId = self.placeId;
    dbGooglePlaceObejct.name = self.name;
    
    return dbGooglePlaceObejct;
}

- (id)initWithGMSAutocompletePrediction:(GMSAutocompletePrediction *)prediction {
    self = [super init];
    
    if (self) {
        self.name = prediction.attributedFullText.string;
        self.placeId = prediction.placeID;
    }
    
    return self;
}

@end
