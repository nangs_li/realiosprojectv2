//
//  REObject.h
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class REDBObject;

@interface REObject : JSONModel

- (nonnull REDBObject *)toREDBObject; // Abstract method. Must be implemented in subclasses if you need to save it into Database

@end
