//
//  RECardInfoViewController.h
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REViewController.h"
#import "REAgentProfile.h"

@interface REAgentListingViewController : REViewController

@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) REAgentProfile *agentProfile;
@property (nonatomic, strong) REAgentListing *agentListing;
@property (nonatomic, assign) CGSize minSize;

+ (REAgentListingViewController *)demoViewController;
- (void)setupWithAgentProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing;

@end
