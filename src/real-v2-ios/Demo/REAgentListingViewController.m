//
//  RECardInfoViewController.m
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REAgentListingViewController.h"
#import <PureLayout/PureLayout.h>
#import "AgentDetailCell.h"
#import <CSStickyHeaderFlowLayout/CSStickyHeaderFlowLayout.h>
#import "FXBlurView.h"
#import "ApartmentDetailCell.h"
#import "AgentProfileCell.h"

#define expandOffsetY 44.0f

@interface REAgentListingViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, AgentDetailCardViewDelegate>

@property (nonatomic, strong) AgentProfileCell *profileCell;
@property (nonatomic, strong) ApartmentDetailCell *apartmentDetailCell;
@property (nonatomic, strong) NSMutableDictionary *frameDict;
@property (nonatomic, assign) BOOL isScrollViewDragFromTop;
@property (nonatomic, strong) NSTimer *scrollTimer;
@property (nonatomic, strong) NSMutableArray *listingImageArray;
@property (nonatomic, strong) NSMutableArray *imageAnimationArray;
@property (strong, nonatomic) NSMutableArray *mwPhotosBrowerPhotoArray;
@property (nonatomic, strong) NSMutableArray *previewReasonLangIndexArray;
@property (nonatomic, strong) NSMutableArray *previewReasonLangTitleArray;
@property (nonatomic, assign) NSNumber *selectedLangIndex;
@property (nonatomic, assign) BOOL backToDetail;
@property (nonatomic, assign) BOOL tableViewInitAnimation;
@property (nonatomic, assign) BOOL backFromOtherVC;
@property (nonatomic, strong) CAShapeLayer *previewArrowLayer;

@end

@implementation REAgentListingViewController

+ (REAgentListingViewController *)demoViewController {
    REAgentListingViewController *demoVC = [[REAgentListingViewController alloc]initWithNibName:@"REAgentListingViewController" bundle:nil];
    
    return demoVC;
}

- (void)setupContentView {
    
    [super setupContentView];
    [self setupCollectionView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    if (isValid(self.agentListing) && isValid(self.agentProfile)) {
        
        [self setupWithAgentProfile:self.agentProfile agentListing:self.agentListing];
    }
    
}

#pragma mark-
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (isValid(self.agentProfile)) {
        
        return  1;
    } else {
        
        return 0;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ApartmentDetailCell *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    self.apartmentDetailCell = cell;
    [cell configureCell:self.agentListing];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return nil;
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        self.profileCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                              withReuseIdentifier:@"header"
                                                                     forIndexPath:indexPath];
        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        self.profileCell.detailProfile.loadCustomBackground = YES;
        [self.profileCell.detailProfile changePageIndex:2];
        self.profileCell.backgroundView = nil;
        [self.profileCell configureCell:self.agentProfile agentListing:self.agentListing];
        
#pragma mark demo only
        
        self.profileCell.frame = CGRectMake(0, 0, [RealUtility screenBounds].size.width, 335);
        
#pragma mark demo only end
        
        //        self.profileCell.detailProfile.backgroundImageView.image = [UIImage imageWithView:self.backgroundImageView];
        //        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        //        self.profileCell.detailProfile.backgroundImageView.layer.transform = self.backgroundImageView.layer.transform;
        return self.profileCell;
    }
    
    return nil;
}

- (BOOL)enoughSpaceForAlphaEffect {
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    //    layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, self.collectionView.frame.size.height -200);
    //    layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
    
    return layout.itemSize.height + layout.parallaxHeaderMinimumReferenceSize.height >= self.collectionView.frame.size.height;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        BOOL needExpandImageTableView = false;
        CGPoint scrollContentOffset = scrollView.contentOffset;
        
        if (scrollContentOffset.y >= profileActionHeaderMinHeight) {
            scrollContentOffset.y = profileActionHeaderMinHeight;
            
        } else if (scrollContentOffset.y <= -expandOffsetY) {
            scrollContentOffset.y = -expandOffsetY;
            needExpandImageTableView = true;
        }
        BOOL enoughSpace = [self enoughSpaceForAlphaEffect];
        [self.profileCell adjustAgentProfileInfoByContentOffset:scrollContentOffset
                                                withAlphaEffect:enoughSpace];
    }
}

- (CGRect)getOriginalFrame:(NSString *)key {
    CGRect frame = CGRectZero;
    NSString *rectString = self.frameDict[key];
    
    if (isValid(rectString)) {
        
        frame = CGRectFromString(rectString);
        
    }
    
    if ([key isEqualToString:@"backgroundImageView"] || [key isEqualToString:@"profileCellBackgroundImageView"]) {
        
        frame = CGRectMake(0, 0, [RealUtility screenBounds].size.width, [RealUtility screenBounds].size.width);
        
    }
    
    return frame;
}

- (void)setupWithAgentProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
    
#pragma mark demo only
    
    CGRect rect = self.collectionView.frame;
    [self.collectionView setFrame:CGRectMake(rect.origin.x, 100, rect.size.width, rect.size.height)];
    
#pragma mark demo only end
    
    self.agentProfile = profile;
    self.agentListing = agentListing;
    
    NSString *bgUrlString = @"";
    
    self.listingImageArray = [NSMutableArray arrayWithArray:self.agentListing.Photos];
    
    if (self.listingImageArray.count > 0) {
        
        [self.listingImageArray removeObjectAtIndex:0];
        
    }
    
    Photos *bgPhoto = [self.listingImageArray firstObject];
    bgUrlString     = bgPhoto.URL;
    NSURL *bgurl    = [NSURL URLWithString:bgUrlString];
    //    bgurl = [NSURL URLWithString:@"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Square_-_black_simple.svg/1000px-Square_-_black_simple.svg.png"];
    
    __weak typeof(self) weakSelf = self;
    [self.backgroundImageView loadImageURL:bgurl
                             withIndicator:YES
                            withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                if (image ) {
                                    weakSelf.backgroundImageView.alpha = 0.0;
                                    [UIView animateWithDuration:1.0
                                                     animations:^{
                                                         weakSelf.backgroundImageView.alpha = 1.0;
                                                     }];
                                }
                                weakSelf.profileCell.contentMode = UIViewContentModeScaleAspectFit;
                                CGRect rect                      = weakSelf.profileCell.detailProfile.backgroundImageView.frame;
                                rect.size                        = [weakSelf getOriginalFrame:@"backgroundImageView"].size;
                                
                                
                            }];
    
    [self.profileCell adjustAgentProfileInfoByContentOffset:CGPointZero withAlphaEffect:YES];
    [self.collectionView setContentOffset:CGPointZero animated:NO];
    [self.collectionView reloadData];
}

- (void)setupCollectionView {
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo([UIScreen mainScreen].bounds.size.height);
        make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width);
        
    }];
    
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ApartmentDetailCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentProfileCell" bundle:nil]forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        
        
        DDLogDebug(@"screen height:%f", [[UIScreen mainScreen]  bounds ].size.height);
        CGFloat scaleFactor = 0.5;
        
        if (is568h) {
            
            scaleFactor = 0.5;
            
        } else {
            
            scaleFactor = 0.6;
            
        }
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, self.collectionView.frame.size.height * scaleFactor);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
        layout.parallaxHeaderAlwaysOnTop = YES;
        CGSize cardSize = [ApartmentDetailCardView calculateSizeWithAgentListing:self.agentListing];
        cardSize.width = [RealUtility screenBounds].size.width;
        CGFloat collectionPreferHeight = [RealUtility screenBounds].size.height - 64;
        
        if (cardSize.height + layout.parallaxHeaderReferenceSize.height < collectionPreferHeight + 8) {
            cardSize.height = collectionPreferHeight - layout.parallaxHeaderReferenceSize.height + 8;
            cardSize.height = collectionPreferHeight - layout.parallaxHeaderReferenceSize.height + 8;
            
        } else {
            
            self.collectionView.contentInset = UIEdgeInsetsZero;
        }
        
        layout.itemSize = cardSize;
    }
    
}

#pragma mark AgentDetailCardViewDelegate

- (void)blankAskButtonDidPress:(REAgentProfile *)profile {
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return YES;
}

@end
