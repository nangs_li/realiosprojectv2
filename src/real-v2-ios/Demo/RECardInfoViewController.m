//
//  RECardInfoViewController.m
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RECardInfoViewController.h"
#import <PureLayout/PureLayout.h>
#import "AgentDetailCell.h"
#import <CSStickyHeaderFlowLayout/CSStickyHeaderFlowLayout.h>
#import "FXBlurView.h"
#import "AgentCell.h"
#import "AgentActionProfileCell.h"

#define expandOffsetY 44.0f

@interface RECardInfoViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, AgentDetailCardViewDelegate ,AgentActionProfileCellDelegate>

@property (nonatomic, strong) AgentActionProfileCell *profileCell;
@property (nonatomic, assign) BOOL DidGoChatRoom;

@end

@implementation RECardInfoViewController

+ (RECardInfoViewController *)demoViewController {
    RECardInfoViewController *demoVC = [[RECardInfoViewController alloc]initWithNibName:@"RECardInfoViewController" bundle:nil];
    
    return demoVC;
}

- (void)setupContentView {
    [super setupContentView];
    
    [self setupCollectionView];
}

- (void)setupCollectionView {
    
    self.collectionView.delegate = self;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.height.mas_equalTo([UIScreen mainScreen].bounds.size.height);
        make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width);
        
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    if (isValid(self.agentListing) && isValid(self.agentProfile)) {
        
        [self setupWithAgentProfile:self.agentProfile agentListing:self.agentListing];
    }

}

#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    if (isValid(self.agentProfile)) {
        
        return 1;
        
    } else {
        
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AgentDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
                                                                      forIndexPath:indexPath];
    [self getCardViewMinSize];
    [cell configureCell:self.agentProfile withMinSize:self.minSize];
    cell.cardView.delegate = self;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        //        CSSectionHeader *cell = [collectionView
        //        dequeueReusableSupplementaryViewOfKind:kind
        //                                                                   withReuseIdentifier:@"sectionHeader"
        //                                                                          forIndexPath:indexPath];
        //
        return nil;
        
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        
        self.profileCell =
        [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                           withReuseIdentifier:@"header"
                                                  forIndexPath:indexPath];
        self.profileCell.detailProfile.loadCustomBackground = YES;
        self.profileCell.delegate = self;
        [self.profileCell.detailProfile changePageIndex:0];
        self.profileCell.backgroundView = nil;
        [self.profileCell configureCell:self.agentProfile agentListing:self.agentListing];
        //        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        //        self.profileCell.detailProfile.backgroundImageView.layer.transform
        //        = self.backgroundImageView.layer.transform;
        return self.profileCell;
    }
    
    return nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        BOOL needExpandImageTableView = false;
        CGPoint scrollContentOffset = scrollView.contentOffset;
        
        if (scrollContentOffset.y >= profileActionHeaderMinHeight) {
            scrollContentOffset.y = profileActionHeaderMinHeight;
            
        } else if (scrollContentOffset.y <= -expandOffsetY) {
            scrollContentOffset.y = -expandOffsetY;
            needExpandImageTableView = true;
        }
        BOOL enoughSpace = [self enoughSpaceForAlphaEffect];
        [self.profileCell adjustAgentProfileInfoByContentOffset:scrollContentOffset
                                                withAlphaEffect:enoughSpace];
    }
}

- (BOOL)enoughSpaceForAlphaEffect {
    CSStickyHeaderFlowLayout *layout =
    (id)self.collectionView.collectionViewLayout;
    //    layout.parallaxHeaderReferenceSize =
    //    CGSizeMake(self.view.frame.size.width,
    //    self.collectionView.frame.size.height -200);
    //    layout.parallaxHeaderMinimumReferenceSize =
    //    CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
    
    return layout.itemSize.height +
    layout.parallaxHeaderMinimumReferenceSize.height >=
    self.collectionView.frame.size.height;
}

- (void)setupWithAgentProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
    
#pragma mark demo only
    
    CGRect  rect = self.collectionView.frame ;
    [self.collectionView setFrame:CGRectMake(rect.origin.x, 64, rect.size.width, rect.size.height)];
    
#pragma mark demo only end
    
    self.agentProfile = profile;
    self.agentListing = agentListing;
    Photos *photos = [agentListing.Photos firstObject];
    NSString *profileImageURL =  photos.URL;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentDetailCell"
                                                    bundle:nil]
          forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentActionProfileCell"
                                                    bundle:nil]
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    CSStickyHeaderFlowLayout *layout =
    (id)self.collectionView.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        
        [self getCardViewMinSize];
        
                __weak typeof(self) weakSelf = self;
                [self.backgroundImageView loadImageURL:[NSURL URLWithString:profileImageURL] withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (image) {
                        UIImage *blurImage = [[UIImage imageWithView:self.backgroundImageView]blurredImageWithRadius:MaxImageBlurRadius iterations:5 tintColor:[UIColor clearColor]];
                        CGPoint offset = CGPointMake(0, blurImage.size.height * 0.75);
                        blurImage = [blurImage add_imageAddingImage:[UIImage imageNamed:@"gradient_mask"] offset:offset];
                        UIImage *layerImage = [UIImage add_imageWithColor:[UIColor colorWithWhite:0 alpha:0.1] size:blurImage.size];
                        blurImage = [blurImage add_imageAddingImage:layerImage];
                        weakSelf.backgroundImageView.image = blurImage;
                        weakSelf.profileCell.detailProfile.backgroundImageView.frame =
                        weakSelf.backgroundImageView.bounds;
                        weakSelf.profileCell.detailProfile.backgroundImageView.image =
                        weakSelf.backgroundImageView.image;
                        
                        if (cacheType == SDImageCacheTypeNone) {
                            weakSelf.backgroundImageView.alpha = 0.0;
                            [UIView animateWithDuration:1.0
                                             animations:^{
        
                                                 weakSelf.backgroundImageView.alpha = 1.0;
        
                                             }];
                        }
                    }
                }];
        
                [self.collectionView setContentOffset:CGPointZero animated:NO];
                [self.profileCell adjustAgentProfileInfoByContentOffset:CGPointZero
                                                        withAlphaEffect:YES];
        
    }
    
    [self.collectionView reloadData];
}

- (void)getCardViewMinSize {
    
    CSStickyHeaderFlowLayout *layout =
    (id)self.collectionView.collectionViewLayout;
    
    CGFloat screenHeight = [RealUtility screenBounds].size.height;
    CGFloat scaleFactor = 0.5;
    
    if (screenHeight >= 667.0f) {
        
        //iphone 6 or above
        scaleFactor = 0.5;
        
    } else if (screenHeight >= 568.0f) {
        
        //iphone 5
        scaleFactor = 0.6;
        
    } else {
        
        //iphone 4
        scaleFactor = 0.7;
    }
    
    layout.parallaxHeaderReferenceSize =
    CGSizeMake(self.view.frame.size.width,
               self.collectionView.frame.size.height * scaleFactor);
    layout.parallaxHeaderMinimumReferenceSize =
    CGSizeMake(self.view.frame.size.width, profileActionHeaderMinHeight);
    layout.parallaxHeaderAlwaysOnTop = YES;
    CGSize cardSize = [AgentDetailCardView calculateSizeWithAgentProfile:self.agentProfile];
    CGFloat collectionPreferHeight = [RealUtility screenBounds].size.height - 64;
    
    if (cardSize.height + layout.parallaxHeaderReferenceSize.height < collectionPreferHeight + 8) {
        
        cardSize.height = collectionPreferHeight - layout.parallaxHeaderReferenceSize.height + 8;
        self.minSize = cardSize;
        
    }
    
    layout.itemSize = cardSize;
    
}

#pragma mark AgentDetailCardViewDelegate

- (void)blankAskButtonDidPress:(REAgentProfile *)profile {
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return YES;
}

@end
