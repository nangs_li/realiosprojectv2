//
//  RESearchBarViewController.h
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REViewController.h"
#import "RECardInfoViewController.h"
#import "MMDrawerController.h"

@interface RESearchBarViewController : REViewController <MMDrawerTouchDelegate>
@property (nonatomic, strong) RECardInfoViewController *leftViewcontroller;
@property (nonatomic, strong) RECardInfoViewController *Rightpageviewcontroller;
@property (nonatomic, strong) MMDrawerController *mmDrawerController;

+ (RESearchBarViewController *)demoViewController;

@end
