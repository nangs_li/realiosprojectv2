//
//  RESearchBarViewController.m
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RESearchBarViewController.h"
#import <PureLayout/PureLayout.h>

#import <GoogleMaps/GoogleMaps.h>
#import "REGooglePlaceObject.h"
#import "RESearchBar.h"
#import "REAddessSearchResponseModel.h"
#import "REAgentListingResponseModel.h"
#import "REAgentProfileResponseModel.h"
#import "REListingCell.h"

@interface RESearchBarViewController () <UITableViewDelegate, UITableViewDataSource, RESearchBarDelegate, RESearchBarDataSource, UITextFieldDelegate>

@property (nonatomic, strong) RESearchBar *searchBar;
@property (nonatomic, strong) UITableView *listingTableView;

@property (nonatomic, strong) NSArray *searchItems;
@property (nonatomic, strong) NSMutableArray<AgentObject *> *agentObjectArray;

@property (nonatomic, strong) NSArray *memberIdPages;

@property (nonatomic, strong) REAddessSearchResponseModel *response;
@property (nonatomic, strong) NSArray *listings;

@property (nonatomic, assign) int currentPage;
@property (nonatomic, assign) BOOL fetchingPage;

@property (nonatomic, strong) NSURLSessionTask *agentListingURLTask;
@property (nonatomic, strong) NSURLSessionTask *agentProfileURLTask;

@end

@implementation RESearchBarViewController

+ (RESearchBarViewController *)demoViewController {
    RESearchBarViewController *vc = [[RESearchBarViewController alloc]initWithNibName:@"RESearchBarViewController" bundle:nil];
    
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.response.AgentProfiles = [[NSMutableArray<REAgentProfile * > alloc]init];
    self.response.AgentListings = [[NSMutableArray<REAgentListing * > alloc]init];
    [self resetSearchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTitleBar {
    UIView *containerView = self.customTitleBarContainer ? self.customTitleBarContainer : self.view;
    [self.searchBar removeFromSuperview];
    
    if (self.customTitleBarContainer) {
        self.searchBar = [[RESearchBar alloc]initWithParent:self.customTitleBarContainer];
    } else {
        self.searchBar = [[RESearchBar alloc]init];
    }
    self.searchBar.dataSource = self;
    self.searchBar.delegate = self;
    self.searchBar.backgroundColor = [UIColor RETopBarBlueColor];
    [containerView addSubview:self.searchBar];
    
    self.titleBar = self.searchBar;
    [self.searchBar autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20];
    [self.searchBar autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.searchBar autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

- (void)setupContentView {
    [super setupContentView];
    
    [self.listingTableView removeFromSuperview];
    self.listingTableView = [UITableView newAutoLayoutView];
    self.listingTableView.dataSource = self;
    self.listingTableView.delegate = self;
    self.listingTableView.estimatedRowHeight = 320;
    [self.listingTableView registerNib:[REListingCell nib] forCellReuseIdentifier:@"REListingCell"];
    self.listingTableView.rowHeight = 320;
    [self.view addSubview:self.listingTableView];
    
    [self.listingTableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.searchBar];
    [self.listingTableView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.listingTableView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [self.listingTableView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

- (void)searchPlace:(NSString *)keyword {
    
    if ([keyword valid]) {
        
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
        
        __weak RESearchBarViewController *weakSelf = self;
        
        [[GMSPlacesClient sharedClient] autocompleteQuery:keyword bounds:nil filter:filter callback:^(NSArray *results, NSError *error) {
            
            if (!error && [results valid]) {
                
                NSMutableArray *items = [[NSMutableArray alloc]init];
                
                for (GMSAutocompletePrediction *predict in [results copy]) {
                    REGooglePlaceObject *placeObject = [[REGooglePlaceObject alloc]initWithGMSAutocompletePrediction:predict];
                    [items addObject:placeObject];
                }
                
                weakSelf.searchItems = [NSArray arrayWithArray:items];
                [weakSelf.searchBar reloadSearchData];
            } else {
                [weakSelf resetSearchData];
            }
            
        }];
    } else {
        [self resetSearchData];
    }
}

- (void)resetSearchData {
    self.searchItems = [[[REDatabase sharedInstance]getLatestPlaceObject]copy];
    
    if (!self.searchItems) {
        self.searchItems = [[NSArray alloc]init];
    }
    
    [self.searchBar reloadSearchData];
}

- (void)fetchProfileAndListingWithPage:(int)page {
    
    if (!self.fetchingPage) {
        self.fetchingPage = YES;
        int itemPerPage = 10;
        NSRange pageRange;
        pageRange.location = page * itemPerPage;
        
        if (pageRange.location + itemPerPage > self.agentObjectArray.count) {
            pageRange.length = self.agentObjectArray.count - pageRange.location;
        } else {
            pageRange.length = itemPerPage;
        }
        
        NSArray<AgentObject *> *fetchPageIds = [self.agentObjectArray subarrayWithRange:pageRange];
        
        __weak RESearchBarViewController *weakSelf = self;
        
        self.agentProfileURLTask = [REAPIManager agentProfileGetWithMemberIDs:fetchPageIds completion:^(REAgentProfileResponseModel *_Nullable response, NSError * _Nullable error) {
            if (!error) {
                for (int i = 0; i < response.AgentProfiles.count; i++) {
                    REAgentProfile *agentProfile = response.AgentProfiles[i];
                    [[REDatabase sharedInstance] createOrUpdateAgentProfile:agentProfile];
                    
                }
                
                weakSelf.agentListingURLTask = [REAPIManager agentLisingGetWithMemberIDs:fetchPageIds completion:^(REAgentListingResponseModel *_Nullable response, NSError * _Nullable error) {
                    
                    if (!error) {
                        for (int i = 0; i < response.AgentListings.count; i++) {
                            REAgentListing *listing = response.AgentListings[i];
                            [[REDatabase sharedInstance] createOrUpdateAgentListing:listing];
                        }
                        
                        weakSelf.response.AgentProfiles =  [[REDatabase sharedInstance]getAgentProfileFromMIDArray:[self.response mIDArrayFromFullRecordSet]];
                        weakSelf.response.AgentListings = [[REDatabase sharedInstance]getAgentListingFromLIDArray:[self.response LIDArrayFromFullRecordSet]];
                        weakSelf.currentPage = page;
                        weakSelf.fetchingPage = NO;
                        [weakSelf.listingTableView reloadData];
                    }
                }];
            }
        }];
    }
}

- (void)resetSearchResult {
    
    if (self.fetchingPage) {
        [self.agentListingURLTask cancel];
        [self.agentProfileURLTask cancel];
        self.fetchingPage = NO;
    }
    
    self.response.AgentProfiles = [[NSMutableArray<REAgentProfile * > alloc]init];
    self.response.AgentListings = [[NSMutableArray<REAgentListing * > alloc]init];
    self.agentObjectArray = [[NSMutableArray<AgentObject * > alloc]init];
    [self.searchBar updateResultCountString:@""];
    
    [self.listingTableView reloadData];
    self.currentPage = 0;
}

- (void)saveGooglePlaceObject:(REGooglePlaceObject *)placeObject {
    [[REDatabase sharedInstance]createOrUpdateGooglePlaceObject:placeObject];
}

- (BOOL)haveMoreItem {
    return self.agentObjectArray.count > self.response.AgentListings.count;
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section] - 1) {
        if ([self haveMoreItem]) {
            [self fetchProfileAndListingWithPage:self.currentPage + 1];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.response.AgentListings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    REListingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"REListingCell"];
    AgentObject *agentObject = self.response.FullRecordSet[indexPath.row];
    REAgentListing *listing = self.response.AgentListings[indexPath.row];
    REAgentProfile *profile = self.response.AgentProfiles[indexPath.row];
    ListingAddress *listingAddress = agentObject.Address.firstObject;
    int listingID = listing.AgentListingID;
    NSString *licenseNumber = profile.LicenseNumber;
    cell.titleLabel.textColor = [UIColor blackColor];
    cell.titleLabel.text = [NSString stringWithFormat:@"licenseNumber: \"%@\"\nListing ID: %d  \nformattedAddress: %@", licenseNumber, listingID, listingAddress.Address];
    Photos *photo = listing.Photos.firstObject;
    
    if (photo.URL) {
        
        [cell.photoView sd_setImageWithURL:[NSURL URLWithString:photo.URL]];
        
    }
    
    return cell;
}

#pragma mark - RESearchBarDelegate

- (void)searchBar:(RESearchBar *)searchBar didSearchKeyword:(NSString *)keyword {
    [self searchPlace:keyword];
}

- (void)searchBar:(RESearchBar *)searchBar didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    REGooglePlaceObject *selectedPlaceObject = self.searchItems[indexPath.row];
    
    __weak RESearchBarViewController *weakSelf = self;
    
    [searchBar updateKeyword:selectedPlaceObject.name];
    
    [REAPIManager addressSearchWithPlaceId:selectedPlaceObject.placeId completion:^(REAddessSearchResponseModel * _Nullable response, NSError * _Nullable error) {
        if (!error && response) {
            
            self.response = response;
            [weakSelf resetSearchResult];
            weakSelf.agentObjectArray = [response.FullRecordSet copy];
            [weakSelf fetchProfileAndListingWithPage:0];
            [weakSelf.searchBar updateResultCountString:[NSString stringWithFormat:@"%d results", (int) weakSelf.agentObjectArray.count]];
            [weakSelf.searchBar endSearching];
            
            
            if (weakSelf.agentObjectArray.count) {
                
                [weakSelf saveGooglePlaceObject:selectedPlaceObject];
                
            }
            
        }
    }];
}

- (void)searchBarDidCancelSearching:(RESearchBar *)searchBar {
    [self resetSearchResult];
    [self resetSearchData];
}

#pragma mark - RESearchBarDataSource

- (NSInteger)numberOfItemInSearchBar:(RESearchBar *)searchBar {
    
    return self.searchItems.count;
}

- (RESearchItemCell *)searchBar:(RESearchBar *)searchBar itemCell:(RESearchItemCell *)itemCell atIndexPath:(NSIndexPath *)indexPath {
    
    REGooglePlaceObject *result = self.searchItems[indexPath.row];
    [itemCell configureWithTitle:result.name];
    
    return itemCell;
}

#pragma mark - MMDrawerTouchDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return ![self.searchBar isSearching];
    
}

- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
            DDLogInfo(@"UIGestureRecognizerStateBegan");
         
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                
                DDLogInfo(@"-panLocationstart%f", panLocation.x);
                CGPoint panLocation = [panGesture locationInView:self.listingTableView];
                NSIndexPath *indexPath =
                [self.listingTableView indexPathForRowAtPoint:panLocation];

                if (isValid(indexPath)) {
                    
                    [self.leftViewcontroller setupWithAgentProfile:self.response.AgentProfiles[indexPath.row] agentListing:self.response.AgentListings[indexPath.row]];
                    [self.Rightpageviewcontroller setupWithAgentProfile:self.response.AgentProfiles[indexPath.row] agentListing:self.response.AgentListings[indexPath.row]];
                    
                }
                
                //// create imageivew
                //
                
//                CGRect rectInTableView = [self.listingTableView rectForRowAtIndexPath:indexPath];
//                CGRect rectInSuperview = [self.listingTableView convertRect:rectInTableView toView:[self.tableView superview]];
//                if (!self.isMiniView) {
//                    rectInSuperview.size.height += 53;
//                    rectInSuperview.origin.y -=53;
//                }
//                [self showSpotlightInRect:rectInSuperview];
//                DDLogDebug(@"cellRect:%@",NSStringFromCGRect(rectInTableView));
                
            }
            break;
            
        case UIGestureRecognizerStateChanged:
            break;
            
        case UIGestureRecognizerStateEnded:
            
            // setTabBarHidden:NO have bug when only not move to other page
            
        case UIGestureRecognizerStateCancelled:
            DDLogInfo(@"-panLocationend%f", panLocation.x);
            DDLogInfo(@"UIGestureRecognizerStateCancelled-");
            
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- two page");
                DDLogInfo(@"self.MMDrawerController.centerContainerView-->%ld",
                          (long)self.mmDrawerController.openSide);
                
            } else if (screenRect.size.width < panLocation.x) {
                
                DDLogInfo(@"UIGestureRecognizerState- three page");
                
            } else if (0 > panLocation.x) {
                
                DDLogInfo(@"UIGestureRecognizerState- one page");
            }
            break;
            
        default:
            break;
    }

    
}

- (void)finishPanGestureCallBack:(UIPanGestureRecognizer *)panGesture {
       
}

@end
