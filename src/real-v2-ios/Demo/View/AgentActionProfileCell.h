//
//  AgentActionProfileCell.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailProfile.h"

#import "RealBouncyButton.h"

@protocol AgentActionProfileCellDelegate<NSObject>

- (void)profileFollowButtonDidPress:(UIButton *)button label:(UILabel *)countLabel;
- (void)profileChatButtonDidPress:(UIButton *)button;

@end

@interface AgentActionProfileCell : UICollectionViewCell <RealBouncyButtonDelegate>
@property (nonatomic, strong) AgentDetailProfile *detailProfile;
@property (nonatomic, strong) id<AgentActionProfileCellDelegate> delegate;
@property (nonatomic, strong) NSMutableDictionary *originalSizeDict;
@property (nonatomic, strong) NSMutableDictionary *originalFontDict;

- (void)configureCell:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing;
- (void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect;

- (IBAction)followButtonDidPress:(id)sender;
- (IBAction)chatButtonDidPress:(id)sender;

@end
