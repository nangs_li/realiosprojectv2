//
//  AgentActionProfileCell.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentActionProfileCell.h"

#import "RealUtility.h"
#import "UIView+Utility.h"

@implementation AgentActionProfileCell

- (void)awakeFromNib {
  // Initialization code
  if (!self.detailProfile) {
    CGRect profileFrame                                     = [RealUtility screenBounds];
    profileFrame.size.height                                = 426.0f;
    self.detailProfile                                      = [AgentDetailProfile initWithType:AgentProfileAction];
    self.detailProfile.frame                                = self.frame;
    self.detailProfile.backgroundImageView.autoresizingMask = 0;
    self.detailProfile.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    self.detailProfile.backgroundImageView.contentMode      = UIViewContentModeTop;

    self.detailProfile.followButton.delegate = self;
    [self.detailProfile.chatButton addTarget:self action:@selector(chatButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [self.detailProfile configureProfile:nil agentListing:nil];

    [self.detailProfile saveOriginalFrames];
    [self.detailProfile saveOriginalFonts];
    self.detailProfile.contentView.frame = profileFrame;
    [self addSubview:self.detailProfile];
  }
}

- (void)configureCell:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
  [self.detailProfile configureProfile:profile agentListing:agentListing];
}

- (void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect {
  [self.detailProfile adjustAgentProfileInfoByContentOffset:contentOffset withAlphaEffect:needAlphaEffect];
}

- (IBAction)chatButtonDidPress:(UIButton *)sender {
  if ([self.delegate respondsToSelector:@selector(profileChatButtonDidPress:)]) {
    [self.delegate profileChatButtonDidPress:sender];
  }
}

#pragma mark - <RealBouncyButtonDelegate>

- (void)buttonDidPressed:(RealBouncyButton *)sender {
  if (sender == self.detailProfile.followButton) {
    if ([self.delegate respondsToSelector:@selector(profileFollowButtonDidPress:label:)]) {
      [self.delegate profileFollowButtonDidPress:sender label:self.detailProfile.followCountLabel];
    }
  }
}

@end
