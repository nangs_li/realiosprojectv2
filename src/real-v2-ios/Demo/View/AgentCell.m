//
//  AgentCell.m
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentCell.h"

@implementation AgentCell

- (void)awakeFromNib {
  // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withFrame:(CGRect)frame withProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
  if (self) {
    self.clipsToBounds = YES;
    self.agentProfile  = profile;
    frame.origin       = CGPointZero;
    self.bounds        = frame;

    if (!self.shimmeringView) {
      self.shimmeringView                             = [[FBShimmeringView alloc] initWithFrame:frame];
      self.shimmeringView.shimmeringAnimationOpacity  = 1.0;
      self.shimmeringView.shimmeringOpacity           = 0.7;
      self.shimmeringView.shimmeringHighlightLength   = 1.0;
      self.shimmeringView.shimmeringBeginFadeDuration = 0.0;
      self.shimmeringView.shimmeringEndFadeDuration   = 0.0;
      [self addSubview:self.shimmeringView];
      UIView *contentView             = [[UIView alloc] initWithFrame:frame];
      self.shimmeringView.contentView = contentView;
    }

    CGRect profileFrame = frame;
      
    if (!self.apartmentDetailCardView) {
      self.apartmentDetailCardView = [[ApartmentDetailCardView alloc] initWithType:ApartmentQuickView withAgentListing:agentListing];
      //            self.apartmentDetailCardView.delegate = self;
      profileFrame.size.height -= self.apartmentDetailCardView.frame.size.height;
      self.apartmentDetailCardView.frame = CGRectMake(0.0f, profileFrame.size.height, frame.size.width, self.apartmentDetailCardView.frame.size.height);
    }

    if (!self.agentProfileView) {
      NSArray *subviewArray                   = [[NSBundle mainBundle] loadNibNamed:@"AgentDetailProfile" owner:self options:nil];
      self.agentProfileView                   = [subviewArray objectAtIndex:0];
      self.agentProfileView.delegate          = self;
      self.agentProfileView.infoView.hidden   = YES;
      self.agentProfileView.frame             = profileFrame;
      self.agentProfileView.contentView.frame = self.agentProfileView.bounds;
      [self.shimmeringView.contentView addSubview:self.agentProfileView];
    }

    //        if (!self.gradientMask) {
    //            self.gradientMask = [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, self.agentProfileView.frame.origin.y +self.agentProfileView.frame.size.height - 90.0f, self.frame.size.width, self.agentProfileView.frame.origin.y +self.agentProfileView.frame.size.height -90.0f)];
    //            self.gradientMask.clipsToBounds = YES;
    //            self.gradientMask.image =[UIImage imageNamed:@"gradient_mask"];
    //        }

    if (!self.agentProfileView.superview) {
      [self.shimmeringView.contentView addSubview:self.agentProfileView];
    }

    if (!self.gradientMask.superview) {
      [self.shimmeringView.contentView addSubview:self.gradientMask];
    }
      
    if (!self.apartmentDetailCardView.superview) {
      [self.shimmeringView.contentView addSubview:self.apartmentDetailCardView];
    }
  }
    
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)configureWithAgentProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
  self.agentProfile = profile;
  [self.agentProfileView configureProfile:profile agentListing:agentListing];
  [self.apartmentDetailCardView configureWithAgentListing:agentListing];
  [self resetLoadingStatus];
}

- (void)adjustViewWithOffset:(CGFloat)offset {
  [self.apartmentDetailCardView adjustQuickViewInfoOffset:offset];
}

- (void)AgentDetailProfileBackgroundImageButtonDidPress {
  //  if (self.agentProfile.readyToShow) {
  if ([self.delegate respondsToSelector:@selector(AgentCellApartmentBackgroundImageView:)]) {
    [self.delegate AgentCellApartmentBackgroundImageView:self.agentProfile];
  }
  //  }
}

- (void)updateImage {
    
  if (isValid(self.agentProfile)) {
    [self.agentProfileView prefetchImage:self.agentProfile];
  }
    
}

- (void)showSlogan:(BOOL)show animated:(BOOL)animated {
  return; /* disable */
  CGFloat alpha = 0.0f;
    
  if (show) {
    alpha = 1.0f;
  } else {
    alpha = 0.0;
  }

  void (^animationBlock)(void) = ^{
    self.agentProfileView.sloganView.alpha = alpha;
  };
    
  if (animated) {
    [UIView animateWithDuration:0.5 animations:animationBlock];
  } else {
    animationBlock();
  }
}

- (void)setShimmering:(BOOL)shimmering {
  self.shimmeringView.shimmering = shimmering;
}

- (void)willEndDisplay {
  [self setShimmering:NO];
  [self.agentProfileView cancelImageOperation];
  [self.apartmentDetailCardView cancelImageOperation];
}

- (void)resetLoadingStatus {
  //    [self.apartmentDetailCardView showLoadingPlaceholder:!self.agentProfile.readyToShow];
  //    [self setShimmering:!self.agentProfile.readyToShow];
}

@end
