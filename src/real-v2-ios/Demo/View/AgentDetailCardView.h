//
//  AgentDetailCardView.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMTagListView.h"
#import "AMTagView.h"

@protocol AgentDetailCardViewDelegate<NSObject>

- (void)blankAskButtonDidPress:(REAgentProfile *)profile;

@end

@interface AgentDetailCardView : REBaseView <AMTagListDelegate>

@property (nonatomic, strong) REAgentProfile *profile;
@property (nonatomic, strong) id<AgentDetailCardViewDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *experienceView;
@property (nonatomic, strong) IBOutlet UIView *languageView;
@property (nonatomic, strong) IBOutlet UIView *specialtyView;
@property (nonatomic, strong) IBOutlet UIView *pastClosingView;
@property (nonatomic, strong) IBOutlet UILabel *licenseLabel;

@property (nonatomic, strong) IBOutlet UIView *blankInfoView;
@property (nonatomic, strong) IBOutlet UILabel *blankTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *blankDescriptionLabel;
@property (nonatomic, strong) IBOutlet UIButton *blankAskButton;

@property (nonatomic, strong) IBOutlet UIButton *experienceTitleButton;
@property (nonatomic, strong) IBOutlet UIButton *languageTitleButton;
@property (nonatomic, strong) IBOutlet UIButton *specialtyTitleButton;
@property (nonatomic, strong) IBOutlet UIButton *pastClosingTitleButton;
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, strong) IBOutlet UILabel *langTagLabel;
@property (nonatomic, strong) IBOutlet UILabel *specialtyTagLabel;
@property (nonatomic, strong) IBOutlet UIImageView *languageBreakLine;
@property (nonatomic, strong) IBOutlet UIImageView *specialtyBreakLine;
@property (nonatomic, strong) NSMutableArray *experienceItemView;
@property (nonatomic, strong) NSMutableArray *pastClosingItemView;
@property (nonatomic, assign) CGSize minSize;

- (id)initWithAgentProfile:(REAgentProfile *)profile;
- (void)configureWithAgentProfile:(REAgentProfile *)profile;
+ (CGSize)calculateSizeWithAgentProfile:(REAgentProfile *)profile;
- (IBAction)blankAskButtonDidPress:(id)sender;

@end
