//
//  AgentDetailCardView.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentDetailCardView.h"
#import "AgentExperienceItemView.h"
#import "AgentPastClosingItemView.h"

#define contentViewWidth [RealUtility screenBounds].size.width - 32

@implementation AgentDetailCardView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (id)initWithAgentProfile:(REAgentProfile *)profile {
    NSArray *nib                     = [[NSBundle mainBundle] loadNibNamed:@"AgentDetailCardView" owner:self options:nil];
    self                             = (AgentDetailCardView *)[nib objectAtIndex:0];  // or if it exists, (MCQView *)[nib objectAtIndex:0];
    CGRect viewFrame                 = self.frame;
    viewFrame.size.width             = [RealUtility screenBounds].size.width;
    self.frame                       = viewFrame;
    self.containerView.frame         = self.bounds;
    self.backgroundImageView.frame   = self.bounds;
    viewFrame.origin.x               = self.contentView.frame.origin.x;
    viewFrame.size.width             = viewFrame.size.width - viewFrame.origin.x * 2;
    self.contentView.frame           = viewFrame;
    self.profile                     = profile;
    self.langTagLabel.textColor      = [UIColor darkGrayColor];
    self.langTagLabel.font           = [UIFont systemFontOfSize:14];
    self.specialtyTagLabel.textColor = [UIColor darkGrayColor];
    self.specialtyTagLabel.font      = [UIFont systemFontOfSize:14];
    [self configureWithAgentProfile:profile];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
    //                                              object:nil];
    return self;
}

- (void)removeOldView {
    for (UIView *view in self.experienceView.subviews) {
        if (view != self.experienceTitleButton) {
            [view removeFromSuperview];
        }
    }
    
    for (UIView *view in self.pastClosingView.subviews) {
        if (view) {
            if (view != self.pastClosingTitleButton) {
                [view removeFromSuperview];
            }
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)configureWithAgentProfile:(REAgentProfile *)profile {
    BOOL profileIsBlank = NO;
    
    if (profile) {
        profileIsBlank = ![profile.Experiences valid] && ![profile.Languages valid] && ![profile.Specialties valid] && ![profile.PastClosings valid];
        //profileIsBlank = YES;
        
        if (profileIsBlank) {
            self.contentView.hidden   = YES;
            self.blankInfoView.hidden = NO;
            
        } else {
            self.contentView.hidden   = NO;
            self.blankInfoView.hidden = YES;
        }
        
        CGRect expFrame       = self.experienceView.frame;
        CGRect langFrame      = self.languageView.frame;
        CGRect specialtyFrame = self.specialtyView.frame;
        CGRect closingFrame   = self.pastClosingView.frame;
        
        self.backgroundImageView.frame = self.bounds;
        self.experienceItemView        = [[NSMutableArray alloc] init];
        self.pastClosingItemView       = [[NSMutableArray alloc] init];
        self.languageBreakLine.hidden  = YES;
        self.specialtyBreakLine.hidden = YES;
        [self removeOldView];
        
        NSString *licenseNumber = profile.LicenseNumber;
        UIView *mostBottomView  = self.experienceTitleButton;
        
        if (profile.Experiences.count > 0) {
            for (int i = 0; i < profile.Experiences.count; i++) {
                Experiences *exp                  = profile.Experiences[i];
                AgentExperienceItemView *itemView = [[AgentExperienceItemView alloc] initWithAgentExperience:exp size:self.experienceView.frame.size withStyle:ItemViewStyleBlack];
                [self.experienceView addSubview:itemView];
                [self.experienceItemView addObject:itemView];
                
                if (i + 1 >= profile.Experiences.count) {
                    itemView.dividerView.image = [UIImage imageNamed:@"content_breakline"];
                    
                } else {
                    itemView.dividerView.image = [UIImage imageNamed:@"dottedline"];
                }
            }
            
            for (int i = 0; i < self.experienceItemView.count; i++) {
                int previous = i - 1;
                
                UIView *previousView = nil;
                
                if (previous >= 0) {
                    previousView = self.experienceItemView[previous];
                    
                } else {
                    previousView = mostBottomView;
                }
                AgentExperienceItemView *currentView = self.experienceItemView[i];
                //    [currentView moveViewTo:previousView direction:RelativeDirectionBottom padding:8];
            }
            AgentExperienceItemView *lastItemView = [self.experienceItemView lastObject];
            expFrame.size.height                  = lastItemView.frame.origin.y + lastItemView.frame.size.height;
            expFrame.origin.y                     = 8;
            self.experienceView.hidden            = NO;
        } else {
            expFrame                   = self.experienceView.frame;
            expFrame.size.height       = 0;
            self.experienceView.hidden = YES;
        }
        
        self.experienceView.frame = expFrame;
        mostBottomView            = self.experienceView;
        
        if (profile.Languages.count > 0) {
            self.languageBreakLine.hidden = NO;
            self.langTagLabel.hidden      = NO;
            self.langTagLabel.text        = [profile languageTagString];
            CGRect labelFrame             = self.langTagLabel.frame;
            labelFrame.origin.x           = 29;
            labelFrame.size               = [self.langTagLabel.text boundingRectWithSize:CGSizeMake(contentViewWidth - 29 * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.langTagLabel.font } context:nil].size;
            self.langTagLabel.frame       = labelFrame;
            mostBottomView                = self.langTagLabel;
            langFrame.size.height         = self.langTagLabel.frame.origin.y + self.langTagLabel.frame.size.height + 16;
            self.languageView.hidden      = NO;
            
        } else {
            self.langTagLabel.hidden = YES;
            langFrame                = self.languageView.frame;
            self.languageView.hidden = YES;
            langFrame.size.height    = 0;
        }
        
        self.languageView.frame = langFrame;
        
        if (profile.Specialties.count > 0) {
            self.specialtyBreakLine.hidden = NO;
            self.specialtyTagLabel.hidden  = NO;
            self.specialtyTagLabel.text    = [profile specialtyTagString];
            CGRect labelFrame              = self.specialtyTagLabel.frame;
            labelFrame.origin.x            = 29;
            labelFrame.size                = [self.specialtyTagLabel.text boundingRectWithSize:CGSizeMake(contentViewWidth - 29 * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.specialtyTagLabel.font } context:nil].size;
            self.specialtyTagLabel.frame   = labelFrame;
            specialtyFrame.size.height     = self.specialtyTagLabel.frame.origin.y + self.specialtyTagLabel.frame.size.height + 16;
            self.specialtyView.hidden      = NO;
            
        } else {
            self.specialtyTagLabel.hidden = YES;
            specialtyFrame                = self.specialtyView.frame;
            specialtyFrame.size.height    = 0;
            self.specialtyView.hidden     = YES;
        }
        self.specialtyView.frame = specialtyFrame;
        
        mostBottomView = self.pastClosingTitleButton;
        
        if (profile.PastClosings.count > 0) {
            for (int i = 0; i < profile.PastClosings.count; i++) {
                PastClosings *closing              = profile.PastClosings[i];
                AgentPastClosingItemView *itemView = [[AgentPastClosingItemView alloc] initWithAgentPastClosing:closing size:CGSizeMake(contentViewWidth, self.pastClosingView.frame.size.height) withStyle:ItemViewStyleBlack];
                [self.pastClosingView addSubview:itemView];
                [self.pastClosingItemView addObject:itemView];
            }
            
            for (int i = 0; i < self.pastClosingItemView.count; i++) {
                int previous = i - 1;
                
                UIView *previousView = nil;
                
                if (previous >= 0) {
                    previousView = self.pastClosingItemView[previous];
                    
                } else {
                    previousView = mostBottomView;
                }
                AgentPastClosingItemView *currentView = self.pastClosingItemView[i];
                [currentView moveViewTo:previousView direction:RelativeDirectionBottom padding:8];
            }
            AgentPastClosingItemView *lastClosingItemView = [self.pastClosingItemView lastObject];
            closingFrame.size.height                      = lastClosingItemView.frame.origin.y + lastClosingItemView.frame.size.height;
            self.pastClosingView.hidden                   = NO;
            
        } else {
            closingFrame                = self.pastClosingView.frame;
            closingFrame.size.height    = 0;
            self.pastClosingView.hidden = YES;
        }
        self.pastClosingView.frame = closingFrame;
        
        if (!self.experienceView.hidden) {
            mostBottomView = self.experienceView;
            
        } else if (!self.languageView.hidden) {
            mostBottomView = self.languageView;
            
        } else if (!self.specialtyView.hidden) {
            mostBottomView = self.specialtyView;
            
        } else if (!self.pastClosingView.hidden) {
            mostBottomView = self.pastClosingView;
            
        } else if (!self.licenseLabel.hidden) {
            mostBottomView = self.licenseLabel;
        }
        
        [mostBottomView setViewAlignmentInSuperView:ViewAlignmentTop padding:8];
        
        if (!self.languageView.hidden && mostBottomView != self.languageView) {
            [self.languageView moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:8];
            mostBottomView = self.languageView;
        }
        
        if (!self.specialtyView.hidden && mostBottomView != self.specialtyView) {
            [self.specialtyView moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:8];
            mostBottomView = self.specialtyView;
        }
        
        if (!self.pastClosingView.hidden && mostBottomView != self.pastClosingView) {
            [self.pastClosingView moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:8];
            mostBottomView = self.pastClosingView;
        }
        
        if ([licenseNumber valid]) {
            self.licenseLabel.hidden = NO;
            //              self.licenseLabel.text = [NSString stringWithFormat:@"%@ %@",JMOLocalizedString(@"common__licese_no", nil),licenseNumber];
            self.licenseLabel.text = licenseNumber;
            
            if (mostBottomView && mostBottomView != self.licenseLabel) {
                [self.licenseLabel moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:8];
            }
            
            CGRect labelFrame       = self.licenseLabel.frame;
            labelFrame.size         = [self.licenseLabel.text boundingRectWithSize:CGSizeMake(contentViewWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.licenseLabel.font } context:nil].size;
            labelFrame.size.width   = contentViewWidth;
            self.licenseLabel.frame = labelFrame;
            [self.licenseLabel setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
            mostBottomView = self.licenseLabel;
            
        } else {
            self.licenseLabel.hidden = YES;
        }
        
        CGRect mostBottomViewFrame = mostBottomView.frame;
        CGRect contentFrame        = self.contentView.frame;
        contentFrame.size.height   = mostBottomViewFrame.origin.y + mostBottomViewFrame.size.height + 8;
        CGRect containerFrame      = self.containerView.frame;
        containerFrame.size.height = contentFrame.origin.y + contentFrame.size.height + 8;
        containerFrame.origin      = CGPointZero;
        
        if (containerFrame.size.height < self.minSize.height) {
            containerFrame.size.height = self.minSize.height;
        }
        
        self.frame                     = containerFrame;
        self.containerView.frame       = containerFrame;
        self.contentView.frame         = contentFrame;
        self.blankInfoView.frame       = self.containerView.bounds;
        self.backgroundImageView.frame = containerFrame;
        [self.specialtyBreakLine setViewAlignmentInSuperView:ViewAlignmentBottom padding:0];
        [self.languageBreakLine setViewAlignmentInSuperView:ViewAlignmentBottom padding:0];
 
#pragma mark demo only
        
        self.experienceView.hidden = YES;
        self.specialtyView.hidden = YES;

#pragma mark demo only end
        
        if (profileIsBlank) {
            BOOL notSelf = YES;
            NSString *title = JMOLocalizedString(@"profile__no_profile", nil);
            NSString *description = @"";
            NSString *askTitle = JMOLocalizedString(@"profile__ask", nil);
            
            if (notSelf) {
                
                self.blankAskButton.hidden = NO;
                description = [NSString stringWithFormat:JMOLocalizedString(@"profile__didnt_enter", nil),profile.MemberName];
                
            } else {
                
                self.blankAskButton.hidden = YES;
                description = JMOLocalizedString(@"profile__update_your_info", nil);
                
            }
            
            self.blankTitleLabel.text = title;
            [self.blankTitleLabel sizeToFit];
            [self.blankTitleLabel setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
            
            self.blankDescriptionLabel.text = description;
            [self.blankDescriptionLabel sizeToFit];
            [self.blankDescriptionLabel setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
            
            [self.blankAskButton setTitle:askTitle forState:UIControlStateNormal];
            [self.blankAskButton sizeToFit];
            
            [self.blankAskButton setBackgroundImage:[[UIImage imageWithBorder:RealBlueColor size:CGSizeMake(20, 20)] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [self.blankAskButton setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
            [self.blankDescriptionLabel setViewAlignmentInSuperView:ViewAlignmentCenter padding:-12];
            [self.blankTitleLabel moveViewTo:self.blankDescriptionLabel direction:RelativeDirectionTop padding:8];
            [self.blankAskButton moveViewTo:self.blankDescriptionLabel direction:RelativeDirectionBottom padding:16];
        }
    }
}

- (BOOL)tagList:(AMTagListView *)tagListView shouldAddTagWithText:(NSString *)text resultingContentSize:(CGSize)size {
    tagListView.frame = CGRectMake(tagListView.frame.origin.x, tagListView.frame.origin.y, tagListView.frame.size.width, size.height);
    
    return YES;
}

+ (CGSize)calculateSizeWithAgentProfile:(REAgentProfile *)profile {
    if (profile) {
        AgentDetailCardView *tempView = [[AgentDetailCardView alloc] initWithAgentProfile:profile];
        
        return tempView.frame.size;
        
    } else {
        return CGSizeZero;
    }
}

- (IBAction)blankAskButtonDidPress:(id)sender {
    if ([self.delegate respondsToSelector:@selector(blankAskButtonDidPress:)]) {
        [self.delegate blankAskButtonDidPress:self.profile];
    }
}

@end
