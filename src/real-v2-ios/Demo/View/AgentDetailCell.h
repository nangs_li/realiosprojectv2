//
//  AgentDetailCell.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailCardView.h"
#import "REAgentProfile.h"

@interface AgentDetailCell : UICollectionViewCell

@property (nonatomic, strong) AgentDetailCardView *cardView;
- (void)configureCell:(REAgentProfile *)profile withMinSize:(CGSize)size;

@end
