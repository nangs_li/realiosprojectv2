//
//  AgentDetailCell.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentDetailCell.h"

@implementation AgentDetailCell

- (void)awakeFromNib {
  // Initialization code
  self.cardView = [[AgentDetailCardView alloc] initWithAgentProfile:nil];
  [self.contentView addSubview:self.cardView];
}

- (void)configureCell:(REAgentProfile *)profile withMinSize:(CGSize)size {
  self.cardView.minSize = size;
  [self.cardView configureWithAgentProfile:profile];
}

@end
