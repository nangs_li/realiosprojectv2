//
//  AgentDetailProfile.h
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ReAgentListing.h"
#import "RealBouncyButton.h"

enum { AgentProfileDetail = 0, AgentProfileDetailWithoutSlogan, AgentProfileDetailWithoutInfo, AgentProfileAction };
typedef NSUInteger AgentProfileType;

@protocol AgentDetailProfileDelegate<NSObject>

- (void)AgentDetailProfileBackgroundImageButtonDidPress;

@end

@interface AgentDetailProfile : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileBottomSpacingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followerCountTopSpacingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sloganTopSpacingConstraint;

@property (nonatomic, assign) AgentProfileType profileType;
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) IBOutlet UIImageView *sloganImageView;
@property (nonatomic, strong) IBOutlet UIImageView *agentProfileImageView;

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *sloganView;
@property (nonatomic, strong) IBOutlet UIView *infoView;
@property (nonatomic, strong) IBOutlet UIView *followerView;
@property (nonatomic, strong) IBOutlet UIView *actionView;
@property (nonatomic, strong) IBOutlet UIView *loadingView;

@property (nonatomic, strong) IBOutlet UILabel *sloganLabel;
@property (nonatomic, strong) IBOutlet UILabel *followCountLabel;
@property (nonatomic, strong) IBOutlet UILabel *agentNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *followTitleLabel;

@property (nonatomic, strong) IBOutlet RealBouncyButton *followButton;
@property (nonatomic, strong) IBOutlet UIButton *chatButton;
@property (nonatomic, strong) IBOutlet UIButton *backgroundButton;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, assign) BOOL loadCustomBackground;
@property (nonatomic, strong) NSMutableDictionary *originalSizeDict;
@property (nonatomic, strong) NSMutableDictionary *originalFontDict;
@property (nonatomic, strong) id<AgentDetailProfileDelegate> delegate;

- (void)changeSlogan:(NSString *)slogan;
- (void)configureProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing;
- (void)changePageIndex:(int)index;
- (void)showLoadingPlaceholder:(BOOL)show;
- (void)cancelImageOperation;

+ (AgentDetailProfile *)initWithType:(AgentProfileType)profileType;
- (void)saveOriginalFrames;
- (void)saveOriginalFonts;
- (void)saveOriginalViewFrame:(UIView *)view key:(NSString *)key;
- (CGRect)getOriginalFrame:(NSString *)key;
- (void)saveOriginalFont:(UIFont *)font key:(NSString *)key;
- (UIFont *)getOriginalFont:(NSString *)key;
- (void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect;
- (void)resetAgentProfileScale;
- (CGFloat)getScaledValueWithOriginal:(CGFloat)original scale:(CGFloat)scale percent:(CGFloat)percent;

- (IBAction)backgroundImageButtonDidPress:(id)sender;
- (void)prefetchImage:(REAgentProfile *)profile;

@end
