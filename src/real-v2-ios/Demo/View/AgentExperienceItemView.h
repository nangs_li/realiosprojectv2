//
//  AgentExperienceItemView.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentExperienceItemView : REBaseView

@property (nonatomic, assign) ItemViewStyle viewStyle;
@property (nonatomic, strong) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *positionLabel;
@property (nonatomic, strong) IBOutlet UILabel *workingPeriodLabel;
@property (nonatomic, strong) IBOutlet UIImageView *dividerView;
@property (nonatomic, assign) CGSize preferedSize;

- (id)initWithAgentExperience:(Experiences *)experience size:(CGSize)size withStyle:(ItemViewStyle)style;
- (void)configureWithExperience:(Experiences *)experience;
+ (CGSize)calViewSizeWithExperience:(Experiences *)experience withInitSize:(CGSize)size withStyle:(ItemViewStyle)style;

@end
