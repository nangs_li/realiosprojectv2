//
//  AgentListSectionHeaderView.m
//  productionreal2
//
//  Created by Alex Hung on 8/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "AgentListSectionHeaderView.h"

@implementation AgentListSectionHeaderView

- (id)initFromXib:(CGRect)frame {
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpLanguageStringmethod) name:setUpLanguageString object:nil];

  self       = [[[NSBundle mainBundle] loadNibNamed:@"AgentListSectionHeaderView" owner:self options:nil] objectAtIndex:0];
  self.frame = frame;

  if (self) {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentWidthConstraint.constant           = frame.size.width;
    self.contentHeightConstraint.constant          = frame.size.height;

    [self.agentProfileButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [self.agentProfileButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.7]] forState:UIControlStateHighlighted];

    //        [self layoutIfNeeded];
  }

  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - setUpLanguageStringmethod

- (void)setUpLanguageStringmethod {
}

- (void)configureWithAgentProfile:(REAgentProfile *)profile agentListing:(REAgentListing *)agentListing {
  [self cancelImageOperation];
  // [self showLoadingPlaceholder:!profile.readyToShow];
  self.followerCountLabel.textColor = RealBlueColor;
  // self.followerTitleLabel.text=JMOLocalizedString(@"common__followers", nil);
  self.headerProfile                                  = profile;
  self.agentProfileImageView.layer.cornerRadius       = self.profileImageHeightConstraint.constant / 2;
  self.agentProfileButton.layer.cornerRadius          = self.profileImageHeightConstraint.constant / 2;
  self.agentProfilePlaceholderView.layer.cornerRadius = self.profileImageHeightConstraint.constant / 2;
  self.agentProfileImageView.clipsToBounds            = YES;

  NSString *profileUrl = profile.PhotoURL;
  NSString *bgImageURL = @"";
  NSString *name       = profile.MemberName;

  NSString *follwerCount = [NSString stringWithFormat:@"%d", profile.FollowerCount];
  NSURL *profilePicUrl   = [NSURL URLWithString:profileUrl];

  if (isValid(agentListing) && isValid(agentListing.Photos)) {
    Photos *bgPhoto = [agentListing.Photos firstObject];
    bgImageURL      = bgPhoto.URL;
  }

  [self.agentProfileImageView loadImageURL:profilePicUrl withIndicator:YES];

  if (profile.IsFollowing) {
    self.agentProfileImageView.borderType = UIImageViewBorderTypeFollowing;
  } else {
    self.agentProfileImageView.borderType = UIImageViewBorderTypeNone;
  }
  self.agentNameLabel.text     = name;
  self.followerCountLabel.text = [RealUtility getDecimalFormatByString:follwerCount withUnit:nil];
}

- (void)adjustWithOffsetY:(CGFloat)offsetY bounceEffect:(CGFloat)bounceScale {
  CGFloat newImageHeight = sectionHeaderProfileImageHeight - offsetY;
  CGFloat minImageHeight = self.frame.size.height - sectionHeaderProfileImageHeight / 2;

  if (newImageHeight >= minImageHeight && newImageHeight != self.profileImageHeightConstraint.constant) {
    self.didPlayBouncing = NO;
    [self.agentProfileImageView.layer removeAllAnimations];
    [self changeProfileImageSize:newImageHeight];

    if (newImageHeight == sectionHeaderProfileImageHeight && !self.didPlayBouncing) {
      self.didPlayBouncing = YES;
      [UIView animateKeyframesWithDuration:0.6
                                     delay:0.0
                                   options:UIViewKeyframeAnimationOptionCalculationModeLinear
                                animations:^{
                                  [UIView addKeyframeWithRelativeStartTime:0.0
                                                          relativeDuration:0.5
                                                                animations:^{
                                                                  self.agentProfileImageView.transform       = CGAffineTransformScale(self.agentProfileImageView.transform, bounceScale, bounceScale);
                                                                  self.agentProfilePlaceholderView.transform = CGAffineTransformScale(self.agentProfileImageView.transform, bounceScale, bounceScale);
                                                                }];
                                  [UIView addKeyframeWithRelativeStartTime:0.3
                                                          relativeDuration:0.7
                                                                animations:^{
                                                                  self.agentProfileImageView.transform       = CGAffineTransformIdentity;
                                                                  self.agentProfilePlaceholderView.transform = CGAffineTransformIdentity;
                                                                }];
                                }
                                completion:nil];
    }
  } else if (newImageHeight < minImageHeight && newImageHeight != self.profileImageHeightConstraint.constant) {
    [self changeProfileImageSize:minImageHeight];
  }
}

- (void)adjustHeaderOffset:(CGFloat)offsetY {
  //  DDLogDebug(@"headerOffset:%f",offsetY);
  if (offsetY < 0) {
    self.agentNameBottomSpacingConstraint.constant = 8;
    self.backgroundTopSpacingConstraint.constant   = 0;
    [self adjustWithOffsetY:0 bounceEffect:1.05];
  } else {
    self.agentNameBottomSpacingConstraint.constant = 8 + offsetY;
    self.backgroundTopSpacingConstraint.constant   = 0 - offsetY;

    if (offsetY > 64 - sectionHeaderProfileImageHeight / 2) {
      CGFloat imageScaleOffset = offsetY - (64 - sectionHeaderProfileImageHeight / 2);
      [self adjustWithOffsetY:imageScaleOffset bounceEffect:1.05];
    } else {
      [self adjustWithOffsetY:0 bounceEffect:1.05];
    }
  }

  if (offsetY < 72) {
    self.profileImageTopPaddingConstraint.constant = 24;
  } else if (offsetY < 80) {
    self.profileImageTopPaddingConstraint.constant = 24 - (offsetY - 72);
  } else {
    self.profileImageTopPaddingConstraint.constant = 24 - (80 - 72);
  }
}

- (void)changeProfileImageSize:(CGFloat)height {
  self.profileImageHeightConstraint.constant          = height;
  self.profileImageWidthConstraint.constant           = height;
  self.agentProfileImageView.layer.cornerRadius       = height / 2;
  self.agentProfileButton.layer.cornerRadius          = height / 2;
  self.agentProfilePlaceholderView.layer.cornerRadius = height / 2;
}

- (IBAction)profileButtonDidPress:(id)sender {
  if ([self.delegate respondsToSelector:@selector(AgentListSectionHeaderProfileDidPress:)]) {
    [self.delegate AgentListSectionHeaderProfileDidPress:self.headerProfile];
  }
}

- (void)showLoadingPlaceholder:(BOOL)show {
  for (UIView *view in self.loadingView) {
    //        if (view.hidden != !show) {
    view.hidden = !show;
    //        }
  }

  for (UIView *view in self.contentView) {
    //        if (view.hidden != show) {
    view.alpha = !show;
    //        }
  }
}

- (void)cancelImageOperation {
  [self.agentProfileImageView cancelImageOperation];
}

@end
