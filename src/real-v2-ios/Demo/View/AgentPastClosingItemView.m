//
//  AgentPastClosingItemView.m
//  productionreal2
//
//  Created by Alex Hung on 10/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentPastClosingItemView.h"

@implementation AgentPastClosingItemView

- (id)initWithAgentPastClosing:(PastClosings *)pastClosing size:(CGSize)size withStyle:(ItemViewStyle)style {
  NSArray *nib       = [[NSBundle mainBundle] loadNibNamed:@"AgentPastClosingItemView" owner:self options:nil];
  self               = (AgentPastClosingItemView *)[nib objectAtIndex:0];  // or if it exists, (MCQView *)[nib objectAtIndex:0];
  self.frame         = CGRectMake(0, 0, size.width, size.height);
  self.preferredSize = size;
  [self changeStlye:style];
  [self configureView:pastClosing];
    
  return self;
}

- (void)changeStlye:(ItemViewStyle)style {
  self.style = style;
    
  if (style == ItemViewStyleWhite) {
    self.addressLabel.textColor = [UIColor whiteColor];
    self.priceLabel.textColor   = [UIColor lightGrayColor];
    self.dateLabel.textColor    = [UIColor lightGrayColor];
  } else if (style == ItemViewStyleBlack) {
    self.addressLabel.textColor = [UIColor colorWithRed:50 / 255.0f green:59 / 255.0f blue:72 / 255.0f alpha:1.0f];
    self.priceLabel.textColor   = [UIColor darkGrayColor];
    self.dateLabel.textColor    = [UIColor darkGrayColor];
  }
}

- (void)configureView:(PastClosings *)pastClosing {
  //    NSString *type = [[SystemSettingModel shared]getSpaceTypeByPropertyType:pastClosing.PropertyType spaceType:pastClosing.SpaceType];
  NSString *price    = pastClosing.SoldPrice;
  NSString *date     = pastClosing.SoldDateString;
  NSString *address  = pastClosing.Address;
  CGRect typeRect    = self.typeLabel.frame;
  CGRect priceRect   = self.priceLabel.frame;
  CGRect dateRect    = self.dateLabel.frame;
  CGRect addressRect = self.addressLabel.frame;

  self.typeLabel.text    = @"house";
  self.priceLabel.text   = price;
  self.dateLabel.text    = date;
  self.addressLabel.text = address;

  CGSize maxLabelSize = CGSizeMake(self.preferredSize.width - 29 * 2, CGFLOAT_MAX);
    
  if (self.style == ItemViewStyleWhite) {
    maxLabelSize = CGSizeMake(self.preferredSize.width - 29 * 2 - 44 - 24, CGFLOAT_MAX);
  }
    
  typeRect.size = [self.typeLabel sizeThatFits:maxLabelSize];
  typeRect.size.width += 10;
  typeRect.size.height += 6;
  priceRect.size   = [self.priceLabel sizeThatFits:maxLabelSize];
  dateRect.size    = [self.dateLabel sizeThatFits:maxLabelSize];
  addressRect.size = [self.addressLabel sizeThatFits:maxLabelSize];

  self.typeLabel.frame    = typeRect;
  self.priceLabel.frame   = priceRect;
  self.dateLabel.frame    = dateRect;
  self.addressLabel.frame = addressRect;

  self.frame = CGRectMake(0, 0, self.preferredSize.width, self.dividerView.frame.origin.y + self.dividerView.frame.size.height + 8);
}

+ (CGSize)calViewSizeWithAgentPastClosing:(PastClosings *)pastClosing withInitSize:(CGSize)size withStyle:(ItemViewStyle)style {
  AgentPastClosingItemView *tempItemView = [[AgentPastClosingItemView alloc] initWithAgentPastClosing:pastClosing size:size withStyle:style];
    
  return tempItemView.frame.size;
}

@end
