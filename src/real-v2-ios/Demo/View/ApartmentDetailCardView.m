//
//  ApartmentDetailCardView.m
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "ApartmentDetailCardView.h"
#import "RealUtility.h"
#import "UIView+Utility.h"
//#import "RealApiClient.h"
//#import "SystemSettingModel.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define quickViewMaxHeight_568h 140.0f
#define quickViewMaxHeight_480h 88.0f
#define addressLabelMaxWidth 150.0f
#define infoViewDefaultTopSpacing 0.0
#define maxButtonSize CGSizeMake(70.0f, 28.0f)

#define shadowColors @[ [UIColor colorWithRed:180 / 255.0f green:180 / 255.0f blue:180 / 255.0f alpha:1.0], [UIColor colorWithRed:219 / 255.0f green:219 / 255.0f blue:219 / 255.0f alpha:1.0] ]

@implementation ApartmentDetailCardView

- (id)initWithType:(ApartmentCardViewType)type withAgentListing:(REAgentListing *)listing {
    
  [[self.potentialImage1 layer] setBorderWidth:2.0f];
  [[self.potentialImage1 layer] setBorderColor:[UIColor whiteColor].CGColor];
  [[self.potentialImage2 layer] setBorderWidth:2.0f];
  [[self.potentialImage2 layer] setBorderColor:[UIColor whiteColor].CGColor];
  [[self.potentialImage3 layer] setBorderWidth:2.0f];
  [[self.potentialImage3 layer] setBorderColor:[UIColor whiteColor].CGColor];
  ApartmentDetailCardView *cardView = nil;
    
  if (type == ApartmentQuickView) {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ApartmentQuickCardView" owner:self options:nil];
    cardView     = (ApartmentDetailCardView *)[nib objectAtIndex:0];
  } else {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ApartmentDetailCardView" owner:self options:nil];
    cardView     = (ApartmentDetailCardView *)[nib objectAtIndex:0];
  }

  self                                      = cardView;  // or if it exists, (MCQView *)[nib objectAtIndex:0];
  self.cardViewType                         = type;
  self.agentListing                         = listing;
  CGRect viewFrame                          = self.frame;
  viewFrame.size.width                      = [UIScreen mainScreen].bounds.size.width;
  self.frame                                = viewFrame;
  self.potentialImageWidth                  = viewFrame.size.width - 16 - 16 - 23 - self.potentialIndicator1.frame.size.width;
  self.potentialImageHeight                 = ceilf(self.potentialImageWidth / 1.5);
  self.quickViewPriceButton.backgroundColor = RealDarkBlueColor;
    
  if (type == ApartmentQuickView) {
    viewFrame.size.height            = quickViewMaxHeight_480h;
    self.apartmentDescView.hidden    = YES;
    self.priceButton.hidden          = YES;
    self.sizeButton.hidden           = YES;
    self.bedButton.hidden            = YES;
    self.bathroomButton.hidden       = YES;
    self.quickViewPriceButton.hidden = NO;
    [self dividerShouldShow:NO];
    self.frame                               = viewFrame;
    self.backgroundHeightConstraint.constant = viewFrame.size.height;
    self.originalBackgroundImageViewHeight   = viewFrame.size.height;
    self.bottomGapImageView.hidden           = NO;
    self.bottomGapImageView.image            = [UIImage add_imageWithGradient:shadowColors size:self.bottomGapImageView.frame.size direction:ADDImageGradientDirectionVertical];
  } else if (type == ApartmentDetailView) {
    self.apartmentDescView.hidden    = NO;
    self.bottomGapImageView.hidden   = YES;
    self.quickViewPriceButton.hidden = YES;
    [self configureWithAgentListing:listing];
    [self dividerShouldShow:YES];
  }

  return self;
}

- (void)dividerShouldShow:(BOOL)show {
  for (UIView *divider in self.dividers) {
    divider.hidden = !show;
  }
}

- (void)configureWithAgentListing:(REAgentListing *)listing {
  if (isValid(listing)) {
    self.contentView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    [self cancelImageOperation];
    [[self.potentialImage1 layer] setBorderWidth:2.0f];
    [[self.potentialImage1 layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[self.potentialImage2 layer] setBorderWidth:2.0f];
    [[self.potentialImage2 layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[self.potentialImage3 layer] setBorderWidth:2.0f];
    [[self.potentialImage3 layer] setBorderColor:[UIColor whiteColor].CGColor];
    self.agentListing     = listing;
    NSString *priceString = [NSString stringWithFormat:@"%@ %@", listing.CurrencyUnit, listing.PropertyPriceFormattedForRoman];
    //        NSString *sizeString = [NSString stringWithFormat:@"%d %@",listing.PropertySize,[[SystemSettingModel shared]getMetricNativeNameBySizeUnitType:listing.SizeUnitType]];
    NSString *bedCount      = [NSString stringWithFormat:@"%d", listing.BedroomCount];
    NSString *bathroomCount = [NSString stringWithFormat:@"%d", listing.BathroomCount];
    //        NSString *apartmentType = [[SystemSettingModel shared]getSpaceTypeByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
    //        NSString *apartmentImage = [[SystemSettingModel shared]getPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType state:nil];
    //        GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
    //        NSString *address = googleAddress.FormattedAddress;
    //        NSString *name = googleAddress.Name;
    //        NSString *location = googleAddress.Location;
    //        location = @"186 ,Very Long Long Long Long Fairview Avenue Trumbull, Very Long Long Long Long State,CT 06611";
    NSString *slogan = JMOLocalizedString(@"create_listing__why_you", nil);
    //        self.apartmentImageView.image = [UIImage imageNamed:apartmentImage];
    if (self.cardViewType == ApartmentQuickView) {
      self.priceButtonWidthConstraint.priority    = 250;
      self.priceButtonTrailingConstraint.constant = 12;
      //            self.apartmentNameLabel.text =name;
      self.apartmentAddressLabel.numberOfLines = 2;
      //            [self.apartmentAddressLabel setText:location];
      //            [self.apartmentTypeLabel setText:apartmentType];
      [self.quickViewPriceButton setTitle:priceString forState:UIControlStateNormal];
      CGRect addressLabelFrame = [self.apartmentAddressLabel.text boundingRectWithSize:CGSizeMake(self.apartmentInfoView.frame.size.width - 16, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.apartmentAddressLabel.font } context:nil];
      self.infoViewTopPaddingConstraint.constant = (quickViewMaxHeight_480h - (addressLabelFrame.size.height + 6 + 18)) / 2 - 12;
    } else {
      self.priceButtonWidthConstraint.priority    = 999;
      self.apartmentAddressLabel.numberOfLines    = 0;
      self.priceButtonTrailingConstraint.constant = 0;
      [self fitTitle:priceString withButton:self.priceButton];
      //            [self fitTitle:sizeString withButton:self.sizeButton];
      [self.bedButton setTitle:bedCount forState:UIControlStateNormal];
      [self.bathroomButton setTitle:bathroomCount forState:UIControlStateNormal];
      //            [self.apartmentTypeLabel setText:apartmentType];
      //            [self.apartmentAddressLabel setText:address];
      //            self.apartmentNameLabel.text = name;
    }
    //        self.apartmentAddressLabel.text = @"Geneva Woods Ear, Nose & Throat 3730 Rhone Circle #203 Anchorage, AK(Alaska) 99508";
    if (self.cardViewType == ApartmentDetailView) {
      self.sloganLabel.textColor = RealBlueColor;
      self.sloganLabel.text      = slogan;
      [self changePotential:[listing getReason]];
    }
    [self layoutIfNeeded];
  }
}

+ (CGSize)calculateSizeWithAgentListing:(REAgentListing *)listing {
  if (listing) {
    ApartmentDetailCardView *tempView = [[ApartmentDetailCardView alloc] initWithType:ApartmentDetailView withAgentListing:listing];
    CGSize cardSize                   = CGSizeMake(tempView.frame.size.width, tempView.backgroundHeightConstraint.constant);
      
    return cardSize;
  } else {
    return CGSizeZero;
  }
}

- (void)changePotential:(NSArray *)potentialArray {
  UILabel *aboveLabel                  = nil;
  self.potentialIndicator1.hidden      = YES;
  self.potentialLabel1.hidden          = YES;
  self.p1HeightConstraint.constant     = 0;
  self.image1HeightConstraint.constant = 0;
  self.p1TopSpaceConstraint.constant   = 0;

  self.potentialIndicator2.hidden      = YES;
  self.potentialLabel2.hidden          = YES;
  self.p2HeightConstraint.constant     = 0;
  self.image2HeightConstraint.constant = 0;
  self.p2TopSpaceConstraint.constant   = 0;

  self.potentialIndicator3.hidden      = YES;
  self.potentialLabel3.hidden          = YES;
  self.p3HeightConstraint.constant     = 0;
  self.image3HeightConstraint.constant = 0;
  self.p3TopSpaceConstraint.constant   = 0;
  CGFloat padding                      = 0;
  for (int i = 0; i < potentialArray.count; i++) {
    NSDictionary *potentialDict     = potentialArray[i];
    UILabel *potentialLabel         = nil;
    UIImageView *potentialImageView = nil;
    UIButton *potentialIndicator    = nil;
    NSString *potentailString       = potentialDict[@"reason"];
    NSString *potentailMediaURL     = potentialDict[@"mediaURL"];
    PHAsset *asset                  = potentialDict[@"asset"];
    NSLayoutConstraint *labelHeightConstraint;
    NSLayoutConstraint *imageHeightConstrait;
    NSLayoutConstraint *labelTopSpaceConstraint;
      
    if (i == 0) {
      potentialLabel          = self.potentialLabel1;
      potentialImageView      = self.potentialImage1;
      potentialIndicator      = self.potentialIndicator1;
      labelHeightConstraint   = self.p1HeightConstraint;
      labelTopSpaceConstraint = self.p1TopSpaceConstraint;
      imageHeightConstrait    = self.image1HeightConstraint;
    } else if (i == 1) {
      potentialLabel          = self.potentialLabel2;
      potentialImageView      = self.potentialImage2;
      potentialIndicator      = self.potentialIndicator2;
      labelHeightConstraint   = self.p2HeightConstraint;
      labelTopSpaceConstraint = self.p2TopSpaceConstraint;
      imageHeightConstrait    = self.image2HeightConstraint;
    } else if (i == 2) {
      potentialLabel          = self.potentialLabel3;
      potentialImageView      = self.potentialImage3;
      potentialIndicator      = self.potentialIndicator3;
      labelHeightConstraint   = self.p3HeightConstraint;
      labelTopSpaceConstraint = self.p3TopSpaceConstraint;
      imageHeightConstrait    = self.image3HeightConstraint;
    }
      
    if (self.agentListing.isPreview) {
      if (asset) {
        CGSize photoSize = [UIScreen mainScreen].bounds.size;
        photoSize.width  = photoSize.width * 2;
        photoSize.height = photoSize.width;
        padding += 8;
        [RealUtility getImageBy:asset
                           size:photoSize
                        handler:^(UIImage *result, NSDictionary *info) {
                          potentialImageView.image = result;
                        }];
        imageHeightConstrait.constant    = self.potentialImageHeight;
        labelTopSpaceConstraint.constant = 8;
        potentialIndicator.hidden        = NO;
      } else {
        imageHeightConstrait.constant    = 0;
        labelTopSpaceConstraint.constant = 2;
      }
    } else {
      if (isValid(potentailMediaURL)) {
        [potentialImageView loadImageURL:[NSURL URLWithString:potentailMediaURL] withIndicator:YES];
        padding += 8;
        imageHeightConstrait.constant    = self.potentialImageHeight;
        labelTopSpaceConstraint.constant = 8;
        potentialIndicator.hidden        = NO;
      } else {
        imageHeightConstrait.constant    = 0;
        labelTopSpaceConstraint.constant = 2;
      }
    }

    if (isValid(potentailString)) {
      if (!aboveLabel) {
        aboveLabel = potentialLabel;
      }
      potentialLabel.hidden          = NO;
      potentialLabel.numberOfLines   = 0;
      CGRect labelFrame              = potentialLabel.frame;
      potentialLabel.text            = potentailString;
      labelFrame.size                = [potentialLabel sizeThatFits:CGSizeMake(self.potentialImageWidth, CGFLOAT_MAX)];
      labelHeightConstraint.constant = labelFrame.size.height;
      potentialIndicator.hidden      = NO;
      padding += 16;
    } else {
      padding += 18;
      labelHeightConstraint.constant = 0;
    }
  }

  CGRect sloganLabelFrame                  = [self.sloganLabel.text boundingRectWithSize:CGSizeMake(self.apartmentDescView.frame.size.width - 16, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.sloganLabel.font } context:nil];
  CGRect addressLabelFrame                 = [self.apartmentAddressLabel.text boundingRectWithSize:CGSizeMake(self.apartmentInfoView.frame.size.width - 16, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : self.apartmentAddressLabel.font } context:nil];
  self.backgroundHeightConstraint.constant = quickViewMaxHeight_568h + self.image1HeightConstraint.constant + self.image2HeightConstraint.constant + self.image3HeightConstraint.constant + self.p1HeightConstraint.constant + self.p2HeightConstraint.constant + self.p3HeightConstraint.constant +
                                             sloganLabelFrame.size.height + addressLabelFrame.size.height + 8 + padding + 16 + 8;

  //    [self.potentialIndicator1 align:self.potentialLabel1 direction:ViewAlignmentTop padding:0];
  //    [self.potentialIndicator2 align:self.potentialLabel2 direction:ViewAlignmentTop padding:0];
  //    [self.potentialIndicator3 align:self.potentialLabel3 direction:ViewAlignmentTop padding:0];
}

- (void)fitTitle:(NSString *)title withButton:(UIButton *)button {
  //    CGRect buttonFrame = button.frame;
  //    buttonFrame.size =maxButtonSize;
  //    button.frame = buttonFrame;
  button.titleLabel.numberOfLines = 2;
  [button setTitle:title forState:UIControlStateNormal];
  //    CGSize buttonFitSize = [button sizeThatFits:maxButtonSize];
  //    buttonFitSize.width += 5;
  //    buttonFrame.size = buttonFitSize;
  //    button.frame = buttonFrame;
  //    [button sizeToFit];
}

- (CGRect)calculateFitFrame {
  CGRect fitFrame      = self.frame;
  fitFrame.size.height = self.apartmentDescView.frame.origin.y + self.apartmentDescView.frame.size.height;
    
  return fitFrame;
    
}

- (void)adjustQuickViewInfoOffset:(CGFloat)offset {
  if (offset <= 0) {
    self.infoViewTopPaddingConstraint.constant = infoViewDefaultTopSpacing;
    self.backgroundHeightConstraint.constant   = self.originalBackgroundImageViewHeight;
  } else {
    self.infoViewTopPaddingConstraint.constant = -offset;
    self.backgroundHeightConstraint.constant   = self.originalBackgroundImageViewHeight - offset;
  }
  self.apartmentAddressLabel.text = [NSString stringWithFormat:@"%f", offset];
}

- (void)prefetchImage:(REAgentListing *)listing {
  NSArray *potential = [listing getReason];
  for (int i = 0; i < potential.count; i++) {
    NSDictionary *potentialDict     = potential[i];
    NSString *potentailMediaURL     = potentialDict[@"mediaURL"];
    UIImageView *potentialImageView = nil;
      
    if (i == 0) {
      potentialImageView = self.potentialImage1;
    } else if (i == 1) {
      potentialImageView = self.potentialImage2;
    } else if (i == 2) {
      potentialImageView = self.potentialImage3;
    }

    //    [potentialImageView loadImageURL:[NSURL URLWithString:potentailMediaURL] withIndicator:YES];
    potentialImageView.image = [self addBorderToImage:potentialImageView.image];
  }
    
}

- (UIImage *)addBorderToImage:(UIImage *)image {
  CGImageRef bgimage = [image CGImage];
  float width        = CGImageGetWidth(bgimage);
  float height       = CGImageGetHeight(bgimage);

  // Create a temporary texture data buffer
  void *data = malloc(width * height * 4);

  // Draw image to buffer
  CGContextRef ctx = CGBitmapContextCreate(data, width, height, 8, width * 4, CGImageGetColorSpace(image.CGImage), kCGImageAlphaPremultipliedLast);
  CGContextDrawImage(ctx, CGRectMake(0, 0, (CGFloat)width, (CGFloat)height), bgimage);

  // Set the stroke (pen) color
  CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);

  // Set the width of the pen mark
  CGFloat borderWidth = (float)width * 0.05;
  CGContextSetLineWidth(ctx, borderWidth);

  // Start at 0,0 and draw a square
  CGContextMoveToPoint(ctx, 0.0, 0.0);
  CGContextAddLineToPoint(ctx, 0.0, height);
  CGContextAddLineToPoint(ctx, width, height);
  CGContextAddLineToPoint(ctx, width, 0.0);
  CGContextAddLineToPoint(ctx, 0.0, 0.0);

  // Draw it
  CGContextStrokePath(ctx);

  // write it to a new image
  CGImageRef cgimage = CGBitmapContextCreateImage(ctx);
  UIImage *newImage  = [UIImage imageWithCGImage:cgimage];
  CFRelease(cgimage);
  CGContextRelease(ctx);

  // auto-released
  return newImage;
}

- (void)showLoadingPlaceholder:(BOOL)show {
  //    if (self.apartmentLoadingView.hidden != !show) {
  self.apartmentLoadingView.hidden = !show;
  //    }
}

- (void)cancelImageOperation {
  [self.potentialImage1 cancelImageOperation];
  [self.potentialImage2 cancelImageOperation];
  [self.potentialImage3 cancelImageOperation];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
