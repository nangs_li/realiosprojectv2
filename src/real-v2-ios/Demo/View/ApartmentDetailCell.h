//
//  ApartmentDetailCell.h
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApartmentDetailCardView.h"

@interface ApartmentDetailCell : UICollectionViewCell

@property (nonatomic, strong) ApartmentDetailCardView *cardView;
- (void)configureCell:(REAgentListing *)listing;

@end
