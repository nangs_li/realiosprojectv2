//
//  ApartmentDetailCell.m
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <PureLayout/PureLayout.h>
#import "ApartmentDetailCell.h"
#import "REAgentProfile.h"

@implementation ApartmentDetailCell

- (void)awakeFromNib {
  // Initialization code
  self.cardView          = [[ApartmentDetailCardView alloc] initWithType:ApartmentDetailView withAgentListing:nil];
  self.frame             = self.cardView.bounds;
  self.contentView.frame = self.cardView.bounds;
  [self.contentView addSubview:self.cardView];
  [self.cardView autoPinEdgesToSuperviewEdges];
}

- (void)configureCell:(REAgentListing *)listing {
  [self.cardView configureWithAgentListing:listing];
}

@end
