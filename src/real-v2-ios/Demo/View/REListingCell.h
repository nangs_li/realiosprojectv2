//
//  RESearchItemCell.h
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface REListingCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *photoView;

+ (UINib *)nib;
- (void)configureWithTitle:(NSString *)title;

@end
