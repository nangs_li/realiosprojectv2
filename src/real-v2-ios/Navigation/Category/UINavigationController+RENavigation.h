//
//  UINavigationController+RENavigation.h
//  real-v2-ios
//
//  Created by Alex Hung on 17/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
@class REDemoPushViewController, RENavigationControllerDelegate, REAPIDemoViewController;

@interface UINavigationController (RENavigation) <UINavigationControllerDelegate>

@property (nonatomic, strong, nullable) RENavigationControllerDelegate *controllerDelegate;

@end
