//
//  UINavigationController+RENavigation.m
//  real-v2-ios
//
//  Created by Alex Hung on 17/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "UINavigationController+RENavigation.h"
#import "UIViewController+RENavigation.h"
#import "RESlidingAnimatedTransitioning.h"
#import "RENavigationControllerDelegate.h"
#import <objc/runtime.h>

static void * RENavigationControllerDelegateKey = &RENavigationControllerDelegateKey;

@implementation UINavigationController (RENavigation)

- (RENavigationControllerDelegate *)controllerDelegate {
    return objc_getAssociatedObject(self, @selector(controllerDelegate));
}

- (void)setControllerDelegate:(RENavigationControllerDelegate *)controllerDelegate {
    objc_setAssociatedObject(self, @selector(controllerDelegate), controllerDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Push Method

- (void)pushVC:(UIViewController *)vc animated:(BOOL)animated {
    if (!self.controllerDelegate) {
        self.controllerDelegate = [[RENavigationControllerDelegate alloc] initWithNavgationController:self];
    }
    self.delegate = self.controllerDelegate;
    [self pushViewController:vc animated:animated];
}

@end
