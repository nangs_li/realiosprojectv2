//
//  UIViewController+RENavigation.m
//  real-v2-ios
//
//  Created by Alex Hung on 17/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "UIViewController+RENavigation.h"
#import "UINavigationController+RENavigation.h"
#import "REBlurAnimatedTransitioning.h"

@implementation UIViewController (RENavigation)

#pragma mark - present method

- (void)presentVC:(UIViewController *)presentedVC animated:(BOOL)animated completion:(void (^ __nullable)(void))completion {
    presentedVC.modalPresentationStyle = UIModalPresentationCustom;
    presentedVC.transitioningDelegate = self;
    [self presentViewController:presentedVC animated:animated completion:completion];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    REBlurAnimatedTransitioning *transition = [[REBlurAnimatedTransitioning alloc] init];
    transition.isAppearing = YES;

    return transition;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    REBlurAnimatedTransitioning *transition = [[REBlurAnimatedTransitioning alloc] init];
    transition.isAppearing = NO;
    
    return transition;
}

@end
