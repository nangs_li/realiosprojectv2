//
//  BlurAnimatedTransitioning.h
//  walue-ios
//
//  Created by Alex Hung on 11/5/2016.
//  Copyright © 2016 walue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface REBlurAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL isAppearing;

@end
