//
//  BlurAnimatedTransitioning.m
//  walue-ios
//
//  Created by Alex Hung on 11/5/2016.
//  Copyright © 2016 walue. All rights reserved.
//

#import "REBlurAnimatedTransitioning.h"
#import "REBlurOverlayView.h"
static NSInteger blurOverlayTag = 30689;

@implementation REBlurAnimatedTransitioning

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *containerView = [transitionContext containerView],
    *fromView = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey].view,
    *toView   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey].view;
    
    CGRect centerRect = containerView.bounds;
    CGRect bottomRect = containerView.bounds;
    bottomRect.origin.y = containerView.bounds.size.height;
    
    __block REBlurOverlayView *blurOverlay;
    
    if (self.isAppearing) {
        blurOverlay = [self blurOverlayWithFrame:containerView.bounds];
        [blurOverlay showBlur:NO];
        [fromView addSubview:blurOverlay];
        [containerView addSubview:toView];
        toView.frame = bottomRect;
        fromView.frame = centerRect;
    } else {
        blurOverlay = [toView viewWithTag:blurOverlayTag];
        [blurOverlay showBlur:YES];
    }
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        if (self.isAppearing) {
            [blurOverlay showBlur:YES];
            toView.frame = centerRect;
        } else {
            [blurOverlay showBlur:NO];
            fromView.frame = bottomRect;
        }
    } completion:^(BOOL finished) {
        if (finished) {
            if (!self.isAppearing) {
                [fromView removeFromSuperview];
                [blurOverlay removeFromSuperview];
                blurOverlay = nil;
            }
            [transitionContext completeTransition:YES];
        }
    }];
    
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return kTransitionDuration;
}

- (REBlurOverlayView *)blurOverlayWithFrame:(CGRect)bounds {
    REBlurOverlayView *overlay = [[REBlurOverlayView alloc] initWithFrame:bounds];
    overlay.tag = blurOverlayTag;
    
    return overlay;
}

@end
