//
//  RENavigationControllerDelegate.h
//  Navigation
//
//  Created by Alex Hung on 21/12/2015.
//  Copyright © 2015 Alex Hung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RENavigationControllerDelegate : NSObject <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UINavigationController *navigationController;
@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactionController;

- (id)initWithNavgationController:(UINavigationController *)navVC;

@end
