//
//  RENavigationControllerDelegate.m
//  Navigation
//
//  Created by Alex Hung on 21/12/2015.
//  Copyright © 2015 Alex Hung. All rights reserved.
//

#import "RENavigationControllerDelegate.h"
#import "RESlidingAnimatedTransitioning.h"

#define transitonVelocityThershold  400.0f
#define transitionDistanceThreshold  ([UIScreen mainScreen].bounds.size.width/3)

@interface RENavigationControllerDelegate ()

@property (nonatomic, strong) UIViewController *leftViewController;
@property (nonatomic, strong) UIViewController *rightViewController;
@property (nonatomic, weak) UIViewController *fromViewController;
@property (nonatomic, weak) UIViewController *toViewController;

@end

@implementation RENavigationControllerDelegate

#pragma mark - Setup
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
    
}

- (void)setup {
    
}

- (id)initWithNavgationController:(UINavigationController *)navVC {
    self = [super init];
    
    if (self) {
        self.navigationController = navVC;
        [self setup];
    }
    
    return self;
}

#pragma mark - UINavigationControllerDelegate

- (nullable id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>)animationController {
    return self.interactionController;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                            animationControllerForOperation:(UINavigationControllerOperation)operation
                                                         fromViewController:(UIViewController *)fromVC
                                                           toViewController:(UIViewController *)toVC {
    self.fromViewController = fromVC;
    self.toViewController = toVC;
    RESlidingAnimatedTransitioning *transitioning = [[RESlidingAnimatedTransitioning alloc] init];
    
    if (operation == UINavigationControllerOperationPush) {
        transitioning.slidingToLeft = NO;
    } else {
        transitioning.slidingToLeft = YES;
    }
    transitioning.navigationControllerOperation = operation;
    
    return transitioning;
}

@end
