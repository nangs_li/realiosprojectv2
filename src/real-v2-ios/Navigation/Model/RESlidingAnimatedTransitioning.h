//
//  SlideAnimatedTransitioning.h
//  productionreal2
//
//  Created by Alex Hung on 30/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RESlidingAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) UINavigationControllerOperation navigationControllerOperation;
@property (nonatomic, assign) BOOL slidingToLeft;

@end