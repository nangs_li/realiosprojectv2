//
//  NSError+RENetworking.h
//  real-v2-ios
//
//  Created by Derek Cheung on 24/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - API Success Block Success Status
static NSInteger const kStatusCodeSuccess = 0;

/** lt will use "We are currently upgrading our service. Please try again later." message **/ //See RealAPI.pdf
#pragma mark - API Error Block Pop Up Error Status
static NSInteger const kStatusCodeInternalServerError = kCFURLErrorBadServerResponse;

static NSInteger const kStatusCodeNetworkFailError = kCFURLErrorNotConnectedToInternet;

static NSInteger const kStatusCodeTimeOutError = kCFURLErrorTimedOut;

#pragma mark - API Error Block Not Pop Up Error Status
static NSString * const kREAPIErrorDomain = @"co.real.api.error";

@interface NSError (RENetworking)

+ (NSError *)createError:(NSInteger)errorCode;

+ (NSError *)networkUnreachableError;

+ (NSError *)internalServerError;

+ (NSError *)timeoutError;

@end
