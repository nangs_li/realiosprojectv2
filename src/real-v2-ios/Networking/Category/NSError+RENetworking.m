//
//  NSError+RENetworking.m
//  real-v2-ios
//
//  Created by Derek Cheung on 24/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "NSError+RENetworking.h"

// Constant
static NSString * const kStatusMessageInternalServerError = @"We are currently upgrading our service. Please try again later.";
static NSString * const kStatusMessageNetworkFailError = @"Your network has failed. Please try again later.";
static NSString * const kStatusMessageTimeOutError = @"Time Out Error";

@implementation NSError (RENetworking)

+ (NSError *)createError:(NSInteger)errorCode {
    
    NSError *error = nil;
    
    if (errorCode != kStatusCodeSuccess) {
        NSMutableDictionary *userInfo  = [[NSMutableDictionary alloc] init];
        
        NSString *message = [NSString stringWithFormat:@"Error %d", (int) errorCode];
        
        switch (errorCode) {
            case kStatusCodeNetworkFailError:
                message = kStatusMessageNetworkFailError;
                break;
            case kStatusCodeInternalServerError:
                message = kStatusMessageInternalServerError;
                break;
            case kStatusCodeTimeOutError:
                message = kStatusMessageInternalServerError;
                break;
            default:
                break;
        }
        
        [userInfo setObject:message forKey:NSLocalizedDescriptionKey];
        
        error = [NSError errorWithDomain:kREAPIErrorDomain code:errorCode userInfo:userInfo];
    }
    
    return error;
}

+ (NSError *)networkUnreachableError {
    
    return [NSError createError:kStatusCodeNetworkFailError];
}

+ (NSError *)internalServerError {
    
    return [NSError createError:kStatusCodeInternalServerError];
}

+ (NSError *)timeoutError {
    
    return [NSError createError:kStatusCodeTimeOutError];
}

@end
