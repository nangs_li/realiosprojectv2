//
//  RealApiClient.h
//  productionreal2
//
//  Created by Alex Hung on 18/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

// Models
#import "REServerResponseModel.h"

@class AFHTTPSessionManager;

typedef void (^REAPICommonBlock) (id _Nullable response,  NSError * _Nullable error);
typedef void (^REAPIProgressBlock) (id _Nonnull progressObject);

@interface REAPIClient : NSObject

/*!
 * HTTPSessionManager
 */
@property (nonatomic, strong) AFHTTPSessionManager * _Nonnull manager;

/*!
 *  Singleton getter method
 *
 *  @return RealApiClient
 */
+ (nonnull instancetype)sharedClient;

/*!
 *  A initialize method of api client
 */
- (void)commonSetup;

+ (BOOL)networkReachable;

/*!
 *  POST request with with specific method, url, paramter, completionBlock, retry times
 *
 *  @param url       Target URL
 *  @param parameter Input parameters
 *  @param completionBlock Completion block
 *  @param retryTimes time of retry
 */
- (nullable NSURLSessionDataTask *)post:(nonnull NSString *)url
                              parameter:(nonnull NSDictionary *)parameter
                              requestID:(nullable NSString *)requestID
                             retryTimes:(NSInteger)retryTimes
                             completion:(nullable REAPICommonBlock)completionBlock
                               progress:(nullable REAPIProgressBlock)progressBlock;

/*!
 *  POST request with with specific method, url, paramter, completionBlock
 *
 *  @param url       Target URL
 *  @param parameter Input parameters
 *  @param completionBlock Completion block
 */
- (nullable NSURLSessionDataTask *)post:(nonnull NSString *)url
                              parameter:(nonnull NSDictionary *)parameter
                              requestID:(nullable NSString *)requestID
                             completion:(nullable REAPICommonBlock)completionBlock
                               progress:(nullable REAPIProgressBlock)progressBlock;

/*!
 *  GET request with with specific method, url, paramter, completionBlock, retry times
 *
 *  @param url       Target URL
 *  @param parameter Input parameters
 *  @param completionBlock Completion block
 *  @param retryTimes time of retry
 */
- (nullable NSURLSessionDataTask *)get:(nonnull NSString *)url
                             parameter:(nonnull NSDictionary *)parameter
                             requestID:(nullable NSString *)requestID
                            retryTimes:(NSInteger)retryTimes
                            completion:(nullable REAPICommonBlock)completionBlock
                              progress:(nullable REAPIProgressBlock)progressBlock;

/*!
 *  GET request with with specific method, url, paramter, completionBlock
 *
 *  @param url       Target URL
 *  @param parameter Input parameters
 *  @param completionBlock Completion block
 */
- (nullable NSURLSessionDataTask *)get:(nonnull NSString *)url
                             parameter:(nonnull NSDictionary *)parameter
                             requestID:(nullable NSString *)requestID
                            completion:(nullable REAPICommonBlock)completionBlock
                              progress:(nullable REAPIProgressBlock)progressBlock;

@end
