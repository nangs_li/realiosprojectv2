//
//  REAddessSearchResponseModel.m
//  real-v2-ios
//
//  Created by Alex Hung on 15/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REAddessSearchResponseModel.h"

@implementation REAddessSearchResponseModel

- (NSMutableArray<NSString *> *)mIDArrayFromFullRecordSet {
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for (AgentObject *agentObject in self.FullRecordSet) {
        
        [array addObject:@(agentObject.MID)];
    }
    
    return array;
}

- (NSMutableArray<NSString *> *)LIDArrayFromFullRecordSet {
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for (AgentObject *agentObject in self.FullRecordSet) {
        
        [array addObject:@(agentObject.LID)];
    }
    
    return array;
    
}

@end

@implementation AgentObject

@end

@implementation ListingAddress

@end


