//
//  REAgentListingResponseModel.h
//  real-v2-ios
//
//  Created by Alex Hung on 15/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REServerResponseModel.h"
#import "REAgentListing.h"

@interface REAgentListingResponseModel : REServerResponseModel

@property (nonatomic, strong) NSMutableArray<REAgentListing> *AgentListings;

@end
