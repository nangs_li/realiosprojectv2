//
//  RealServerResponseModel.h
//  productionreal2
//
//  Created by Alex Hung on 19/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "REObject.h"

@interface REServerResponseModel : REObject

@property (nonatomic, assign) NSInteger ErrorCode;
@property (nonatomic, strong) NSString *ApiName;
@property (nonatomic, strong) NSString *UniqueKey;
@property (nonatomic, strong) NSString *ErrorMsg;

@end
