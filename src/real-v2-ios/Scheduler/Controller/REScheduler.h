//
//  REScheduler.h
//  real-v2-ios
//
//  Created by Alex Hung on 23/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

@interface REScheduler : NSObject

+ (instancetype)shareScheduler;
- (void)setup;

- (void)removeScheduleWithKey:(NSString *)key;
- (void)scheduleWithKey:(NSString *)key timeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))block repeats:(BOOL)repeats;

@end
