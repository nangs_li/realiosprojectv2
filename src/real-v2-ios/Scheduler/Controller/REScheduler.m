//
//  REScheduler.m
//  real-v2-ios
//
//  Created by Alex Hung on 23/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REScheduler.h"

@interface REScheduler ()

@property (nonatomic, strong) NSMutableDictionary *timerDict;

@end

@implementation REScheduler

static REScheduler * _shareScheduler = nil;

+ (instancetype)shareScheduler {
    
    if (_shareScheduler == nil) {
        _shareScheduler = [[self alloc] init];
        
        if (_shareScheduler) {
            [_shareScheduler setup];
        }
        
    }
    
    return _shareScheduler;
}

- (void)setup {
    _timerDict = [[NSMutableDictionary alloc] init];
}

- (void)removeScheduleWithKey:(NSString *)key {
    if ([[_timerDict objectForKey:key] isKindOfClass:[NSTimer class]]) {
        NSTimer *timer = _timerDict[key];
        [timer invalidate];
        [_timerDict removeObjectForKey:key];
    }
}

- (void)scheduleWithKey:(NSString *)key timeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))block repeats:(BOOL)repeats {
    [self removeScheduleWithKey:key];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:[NSBlockOperation blockOperationWithBlock:block] selector:@selector(main) userInfo:nil repeats:repeats];
    [_timerDict setObject:timer forKey:key];
}

@end
