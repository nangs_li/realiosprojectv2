//
//  NSDictionary+Utility.h
//  real-v2-ios
//
//  Created by Alex Hung on 25/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utility)

- (NSString *)jsonString;
+ (NSDictionary *)dictionaryFromJsonString:(NSString *)jsonString;

@end
