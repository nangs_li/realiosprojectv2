//
//  NSString+Utility.h
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

- (NSString *)trim;

@end
