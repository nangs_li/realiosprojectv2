//
//  UIColor+REColor.h
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (REColor)

+ (UIColor *)RETopBarBlueColor;

+ (UIColor *)REDimBackgroundColor;

@end
