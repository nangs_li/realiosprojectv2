//
//  UIImageView+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 31/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UIImage+Utility.h"

@interface NSString (MD5Hashing)

- (NSString *)md5;

@end

#import <CommonCrypto/CommonDigest.h>

@implementation NSString (MD5Hashing)

- (NSString *)md5 {
    
    const char * cStr = [self UTF8String];
    unsigned char result[16];
    
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
}

@end

NSString * NSStringFromADDCornerInset(ADDCornerInset cornerInset) { return [NSString stringWithFormat:@"ADDCornerInset <topLeft:%f> <topRight:%f> <bottomLeft:%f> <bottomRight:%f>", cornerInset.topLeft, cornerInset.topRight, cornerInset.bottomLeft, cornerInset.bottomRight]; }

const ADDCornerInset ADDCornerInsetZero = {0.0f, 0.0f, 0.0f, 0.0f};
static NSCache * _imageCache             = nil;

static NSString * kUIImageName               = @"kUIImageName";
static NSString * kUIImageResizableImage     = @"kUIImageResizableImage";
static NSString * kUIImageColors             = @"kUIImageColors";
static NSString * kUIImageTintColor          = @"kUIImageTintColor";
static NSString * kUIImageTintStyle          = @"kUIImageTintStyle";
static NSString * kUIImageCornerInset        = @"kUIImageCornerInset";
static NSString * kADDImageGradientDirection = @"kADDImageGradientDirection";
static NSString * kUIImageSize               = @"kUIImageSize";

@implementation UIImage (Utility)

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
+ (UIImage *)imageWithVeritcalDot:(CGSize)dotSize padding:(CGFloat)padding imageSize:(CGSize)imageSize {
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(imageSize.width / 2, padding)];
    [path addLineToPoint:CGPointMake(padding, imageSize.height)];
    [path setLineWidth:imageSize.height];
    CGFloat dashes[] = {path.lineWidth, path.lineWidth * 2};
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapRound];
    UIGraphicsBeginImageContextWithOptions(imageSize, false, 2);
    [path stroke];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageWithDot:(CGSize)dotSize padding:(CGFloat)padding imageSize:(CGSize)imageSize {
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(padding, imageSize.height / 2)];
    [path addLineToPoint:CGPointMake(imageSize.width, padding)];
    [path setLineWidth:imageSize.width];
    CGFloat dashes[] = {path.lineWidth, path.lineWidth * 2};
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapRound];
    UIGraphicsBeginImageContextWithOptions(imageSize, false, 2);
    [path stroke];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 10.0f, 10.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageWithBorder:(UIColor *)color size:(CGSize)size;
{
    UIGraphicsBeginImageContext(size);
    UIImage *clearImage = [UIImage imageWithColor:[UIColor clearColor]];
    CGRect rect         = CGRectMake(0, 0, size.width, size.height);
    [clearImage drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextStrokeRect(context, rect);
    UIImage *testImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [testImg resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)];
}

+ (UIImage *)imageWithBorder:(UIColor *)borderColor size:(CGSize)size fillcolor:(UIColor *)fillColor {
    UIGraphicsBeginImageContext(size);
    UIImage *clearImage = [UIImage imageWithColor:[UIColor clearColor]];
    CGRect rect         = CGRectMake(0, 0, size.width, size.height);
    [clearImage drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextFillRect(context, rect);
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, borderColor.CGColor);
    CGContextStrokeRect(context, rect);
    
    UIImage *testImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [testImg resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)];
}

+ (UIImage *)imageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *)fixOrientation {
    // No-op if the orientation is already correct.
    if (self.imageOrientation == UIImageOrientationUp) {
        return self;
    }
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height, CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context.
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img     = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    
    return img;
}

- (UIImage *)rotateByAngle:(CGFloat)angleInRadians {
    CGSize contextSize = self.size;
    
    UIGraphicsBeginImageContextWithOptions(contextSize, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0.5 * contextSize.width, 0.5 * contextSize.height);
    CGContextRotateCTM(context, angleInRadians);
    CGContextTranslateCTM(context, -0.5 * contextSize.width, -0.5 * contextSize.height);
    [self drawAtPoint:CGPointZero];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)add_imageWithColor:(UIColor *)color {
    return [self add_imageWithColor:color size:CGSizeMake(1, 1)];
}

+ (UIImage *)add_imageWithColor:(UIColor *)color size:(CGSize)size {
    return [self add_imageWithColor:color size:size cornerInset:ADDCornerInsetZero];
}

+ (UIImage *)add_imageWithColor:(UIColor *)color size:(CGSize)size cornerRadius:(CGFloat)cornerRadius {
    return [self add_imageWithColor:color size:size cornerInset:ADDCornerInsetMake(cornerRadius, cornerRadius, cornerRadius, cornerRadius)];
}

+ (UIImage *)add_imageWithColor:(UIColor *)color size:(CGSize)size cornerInset:(ADDCornerInset)cornerInset {
    return [self add_imageWithColor:color size:size cornerInset:cornerInset saveInCache:YES];
}

+ (UIImage *)add_resizableImageWithColor:(UIColor *)color {
    return [self add_resizableImageWithColor:color cornerInset:ADDCornerInsetZero];
}

+ (UIImage *)add_resizableImageWithColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius {
    return [self add_resizableImageWithColor:color cornerInset:ADDCornerInsetMake(cornerRadius, cornerRadius, cornerRadius, cornerRadius)];
}

+ (UIImage *)add_resizableImageWithColor:(UIColor *)color cornerInset:(ADDCornerInset)cornerInset {
    
    if (!color) {
        
        return nil;
    }
    
    NSDictionary *descriptors = @{ kUIImageColors : @[ color ], kUIImageResizableImage : @YES, kUIImageCornerInset : [NSValue valueWithADDCornerInset:cornerInset] };
    
    UIImage *image = [self add_cachedImageWithDescriptors:descriptors];
    
    if (image)
        
        return image;
    
    CGSize size = CGSizeMake(MAX(cornerInset.topLeft, cornerInset.bottomLeft) + MAX(cornerInset.topRight, cornerInset.bottomRight) + 1, MAX(cornerInset.topLeft, cornerInset.topRight) + MAX(cornerInset.bottomLeft, cornerInset.bottomRight) + 1);
    
    UIEdgeInsets capInsets = UIEdgeInsetsMake(MAX(cornerInset.topLeft, cornerInset.topRight), MAX(cornerInset.topLeft, cornerInset.bottomLeft), MAX(cornerInset.bottomLeft, cornerInset.bottomRight), MAX(cornerInset.topRight, cornerInset.bottomRight));
    
    image = [[self add_imageWithColor:color size:size cornerInset:cornerInset] resizableImageWithCapInsets:capInsets];
    
    [self add_cacheImage:image withDescriptors:descriptors];
    
    return image;
}

+ (UIImage *)add_blackColorImage {
    return [self add_resizableImageWithColor:[UIColor blackColor]];
}

+ (UIImage *)add_darkGrayColorImage {
    return [self add_resizableImageWithColor:[UIColor darkGrayColor]];
}

+ (UIImage *)add_lightGrayColorImage {
    return [self add_resizableImageWithColor:[UIColor lightGrayColor]];
}

+ (UIImage *)add_whiteColorImage {
    return [self add_resizableImageWithColor:[UIColor whiteColor]];
}

+ (UIImage *)add_grayColorImage {
    return [self add_resizableImageWithColor:[UIColor grayColor]];
}

+ (UIImage *)add_redColorImage {
    return [self add_resizableImageWithColor:[UIColor redColor]];
}

+ (UIImage *)add_greenColorImage {
    return [self add_resizableImageWithColor:[UIColor greenColor]];
}

+ (UIImage *)add_blueColorImage {
    return [self add_resizableImageWithColor:[UIColor blueColor]];
}

+ (UIImage *)add_cyanColorImage {
    return [self add_resizableImageWithColor:[UIColor cyanColor]];
}

+ (UIImage *)add_yellowColorImage {
    return [self add_resizableImageWithColor:[UIColor yellowColor]];
}

+ (UIImage *)add_magentaColorImage {
    return [self add_resizableImageWithColor:[UIColor magentaColor]];
}

+ (UIImage *)add_orangeColorImage {
    return [self add_resizableImageWithColor:[UIColor orangeColor]];
}

+ (UIImage *)add_purpleColorImage {
    return [self add_resizableImageWithColor:[UIColor purpleColor]];
}

+ (UIImage *)add_brownColorImage {
    return [self add_resizableImageWithColor:[UIColor brownColor]];
}

+ (UIImage *)add_clearColorImage {
    return [self add_resizableImageWithColor:[UIColor clearColor]];
}

+ (UIImage *)add_imageNamed:(NSString *)name tintColor:(UIColor *)color style:(ADDImageTintStyle)tintStyle {
    
    if (!name) {
        
        return nil;
    }
    
    UIImage *image = [UIImage imageNamed:name];
    
    if (!image) {
        
        return nil;
    }
    
    if (!color) {
        
        return image;
    }
    
    NSDictionary *descriptors = @{ kUIImageName : name, kUIImageTintColor : color, kUIImageTintStyle : @(tintStyle) };
    
    UIImage *tintedImage = [self add_cachedImageWithDescriptors:descriptors];
    
    if (!tintedImage) {
        tintedImage = [image add_tintedImageWithColor:color style:tintStyle];
        [self add_cacheImage:tintedImage withDescriptors:descriptors];
    }
    
    return tintedImage;
}

- (UIImage *)add_tintedImageWithColor:(UIColor *)color style:(ADDImageTintStyle)tintStyle {
    
    if (!color) {
        
        return self;
    }
    
    CGFloat scale = self.scale;
    CGSize size   = CGSizeMake(scale * self.size.width, scale * self.size.height);
    
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    // ---
    
    if (tintStyle == ADDImageTintStyleKeepingAlpha) {
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        CGContextDrawImage(context, rect, self.CGImage);
        CGContextSetBlendMode(context, kCGBlendModeSourceIn);
        [color setFill];
        CGContextFillRect(context, rect);
    } else if (tintStyle == ADDImageTintStyleOverAlpha) {
        [color setFill];
        CGContextFillRect(context, rect);
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        CGContextDrawImage(context, rect, self.CGImage);
    } else if (tintStyle == ADDImageTintStyleOverAlphaExtreme) {
        [color setFill];
        CGContextFillRect(context, rect);
        CGContextSetBlendMode(context, kCGBlendModeDestinationOut);
        CGContextDrawImage(context, rect, self.CGImage);
    }
    
    // ---
    CGImageRef bitmapContext = CGBitmapContextCreateImage(context);
    
    UIImage *coloredImage = [UIImage imageWithCGImage:bitmapContext scale:scale orientation:UIImageOrientationUp];
    
    CGImageRelease(bitmapContext);
    
    UIGraphicsEndImageContext();
    
    return coloredImage;
}

- (UIImage *)add_imageWithRoundedBounds {
    CGSize size    = self.size;
    CGFloat radius = MIN(size.width, size.height) / 2.0;
    
    return [self add_imageWithCornerRadius:radius];
}

- (UIImage *)add_imageWithCornerRadius:(CGFloat)cornerRadius {
    return [self add_imageWithCornerInset:ADDCornerInsetMake(cornerRadius, cornerRadius, cornerRadius, cornerRadius)];
}

- (UIImage *)add_imageWithCornerInset:(ADDCornerInset)cornerInset {
    
    if (![self add_isValidCornerInset:cornerInset]) {
        
        return nil;
    }
    
    CGFloat scale = self.scale;
    
    CGRect rect = CGRectMake(0.0f, 0.0f, scale * self.size.width, scale * self.size.height);
    
    cornerInset.topRight *= scale;
    cornerInset.topLeft *= scale;
    cornerInset.bottomLeft *= scale;
    cornerInset.bottomRight *= scale;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context       = CGBitmapContextCreate(NULL, rect.size.width, rect.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    
    if (context == NULL)
        
        return nil;
    
    CGFloat minx = CGRectGetMinX(rect), midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect);
    CGFloat miny = CGRectGetMinY(rect), midy = CGRectGetMidY(rect), maxy = CGRectGetMaxY(rect);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, cornerInset.bottomLeft);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, cornerInset.bottomRight);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, cornerInset.topRight);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, cornerInset.topLeft);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGContextDrawImage(context, rect, self.CGImage);
    
    CGImageRef bitmapImageRef = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    
    UIImage *newImage = [UIImage imageWithCGImage:bitmapImageRef scale:scale orientation:UIImageOrientationUp];
    
    CGImageRelease(bitmapImageRef);
    
    return newImage;
}

- (BOOL)add_isValidCornerInset:(ADDCornerInset)cornerInset {
    CGSize size = self.size;
    
    BOOL isValid = YES;
    
    if (cornerInset.topLeft + cornerInset.topRight > size.width) {
        
        isValid = NO;
        
    } else if (cornerInset.topRight + cornerInset.bottomRight > size.height) {
        
        isValid = NO;
    } else if (cornerInset.bottomRight + cornerInset.bottomLeft > size.width) {
        
        isValid = NO;
        
    } else if (cornerInset.bottomLeft + cornerInset.topLeft > size.height)
        isValid = NO;
    
    return isValid;
}

- (UIImage *)add_imageAddingImage:(UIImage *)image {
    CGSize size1 = self.size;
    CGSize size2 = image.size;
    
    CGPoint offset = CGPointMake(floorf((size1.width - size2.width) / 2.0), floorf((size1.height - size2.height) / 2.0));
    
    return [self add_imageAddingImage:image offset:offset];
}

- (UIImage *)add_imageAddingImage:(UIImage *)image offset:(CGPoint)offset {
    CGSize size   = self.size;
    CGFloat scale = self.scale;
    
    size.width *= scale;
    size.height *= scale;
    
    UIGraphicsBeginImageContext(size);
    
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    [image drawInRect:CGRectMake(scale * offset.x, scale * offset.y, image.size.width * scale, image.size.height * scale)];
    
    CGContextRef context     = UIGraphicsGetCurrentContext();
    CGImageRef bitmapContext = CGBitmapContextCreateImage(context);
    UIImage *destImage       = [UIImage imageWithCGImage:bitmapContext scale:image.scale orientation:UIImageOrientationUp];
    UIGraphicsEndImageContext();
    CGImageRelease(bitmapContext);
    
    return destImage;
}

#pragma mark Private Methods

+ (NSCache *)add_cache {
    
    if (!_imageCache) {
        
        _imageCache = [[NSCache alloc] init];
    }
    
    return _imageCache;
}

+ (UIImage *)add_cachedImageWithDescriptors:(NSDictionary *)descriptors {
    return [[self add_cache] objectForKey:[self add_keyForImageWithDescriptors:descriptors]];
}

+ (void)add_cacheImage:(UIImage *)image withDescriptors:(NSDictionary *)descriptors {
    NSString *key = [self add_keyForImageWithDescriptors:descriptors];
    [[self add_cache] setObject:image forKey:key];
}

+ (NSString *)add_keyForImageWithDescriptors:(NSDictionary *)descriptors {
    NSMutableString *string = [NSMutableString string];
    
    NSString *imageName = [descriptors valueForKey:kUIImageName];
    [string appendFormat:@"<%@:%@>", kUIImageName, (imageName == nil) ? @"" : imageName];
    [string appendFormat:@"<%@:%@>", kUIImageSize, NSStringFromCGSize([[descriptors valueForKey:kUIImageSize] CGSizeValue])];
    [string appendFormat:@"<%@:%d>", kUIImageResizableImage, [[descriptors valueForKey:kUIImageResizableImage] boolValue]];
    
    [string appendFormat:@"<%@:", kUIImageColors];
    NSArray *colors = [descriptors valueForKey:kUIImageColors];
    for (UIColor *color in colors) [string appendFormat:@"%ld", (long)color.hash];
    [string appendFormat:@">"];
    
    [string appendFormat:@"<%@:%ld>", kUIImageTintColor, (long)[[descriptors valueForKey:kUIImageTintColor] hash]];
    [string appendFormat:@"<%@:%ld>", kUIImageTintStyle, (long)[[descriptors valueForKey:kUIImageTintStyle] integerValue]];
    [string appendFormat:@"<%@:%@>", kUIImageCornerInset, NSStringFromADDCornerInset([[descriptors valueForKey:kUIImageCornerInset] ADDCornerInsetValue])];
    [string appendFormat:@"<%@:%ld>", kADDImageGradientDirection, (long)[[descriptors valueForKey:kADDImageGradientDirection] integerValue]];
    
    return [string md5];
}

+ (UIImage *)add_imageWithColor:(UIColor *)color size:(CGSize)size cornerInset:(ADDCornerInset)cornerInset saveInCache:(BOOL)save {
    NSDictionary *descriptors = @{ kUIImageColors : @[ color ], kUIImageSize : [NSValue valueWithCGSize:size], kUIImageCornerInset : [NSValue valueWithADDCornerInset:cornerInset] };
    
    UIImage *image = [self add_cachedImageWithDescriptors:descriptors];
    
    if (image) {
       
        return image;
        
    }
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGRect rect   = CGRectMake(0.0f, 0.0f, scale * size.width, scale * size.height);
    
    cornerInset.topRight *= scale;
    cornerInset.topLeft *= scale;
    cornerInset.bottomLeft *= scale;
    cornerInset.bottomRight *= scale;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context       = CGBitmapContextCreate(NULL, rect.size.width, rect.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGColorSpaceRelease(colorSpace);
    
    if (context == NULL) {
        
        return nil;
    }
    
    CGFloat minx = CGRectGetMinX(rect), midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect);
    CGFloat miny = CGRectGetMinY(rect), midy = CGRectGetMidY(rect), maxy = CGRectGetMaxY(rect);
    
    CGContextBeginPath(context);
    CGContextSetGrayFillColor(context, 1.0, 0.0);  // <-- Alpha color in background
    CGContextAddRect(context, rect);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFill);
    
    CGContextSetFillColorWithColor(context, [color CGColor]);  // <-- Color to fill
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, cornerInset.bottomLeft);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, cornerInset.bottomRight);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, cornerInset.topRight);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, cornerInset.topLeft);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFill);
    
    CGImageRef bitmapContext = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    
    UIImage *theImage = [UIImage imageWithCGImage:bitmapContext scale:scale orientation:UIImageOrientationUp];
    
    CGImageRelease(bitmapContext);
    
    if (save) {
        
        [self add_cacheImage:theImage withDescriptors:descriptors];
    }
    
    return theImage;
}

+ (UIImage *)add_imageWithGradient:(NSArray *)colors size:(CGSize)size direction:(ADDImageGradientDirection)direction {
    NSDictionary *descriptors = @{ kUIImageColors : colors, kUIImageSize : [NSValue valueWithCGSize:size], kADDImageGradientDirection : @(direction) };
    
    UIImage *image = [self add_cachedImageWithDescriptors:descriptors];
    
    if (image) {
        
        return image;
    }
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create Gradient
    NSMutableArray *cgColors = [NSMutableArray arrayWithCapacity:colors.count];
    for (UIColor *color in colors) [cgColors addObject:(id)color.CGColor];
    
    CGColorSpaceRef space  = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)cgColors, NULL);
    
    // Apply gradient
    CGPoint startPoint = CGPointZero;
    CGPoint endPoint   = CGPointZero;
    
    if (direction == ADDImageGradientDirectionVertical) {
        endPoint = CGPointMake(0, rect.size.height);
    } else if (direction == ADDImageGradientDirectionHorizontal) {
        endPoint = CGPointMake(rect.size.width, 0);
    } else if (direction == ADDImageGradientDirectionLeftSlanted) {
        endPoint = CGPointMake(rect.size.width, rect.size.height);
    } else if (direction == ADDImageGradientDirectionRightSlanted) {
        startPoint = CGPointMake(rect.size.width, 0);
        endPoint   = CGPointMake(0, rect.size.height);
    }
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Clean memory & End context
    UIGraphicsEndImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(space);
    
    [self add_cacheImage:image withDescriptors:descriptors];
    
    return image;
}

+ (UIImage *)add_resizableImageWithGradient:(NSArray *)colors size:(CGSize)size direction:(ADDImageGradientDirection)direction {
    
    if ((size.width == 0.0f && direction == ADDImageGradientDirectionHorizontal) || (size.height == 0.0f && direction == ADDImageGradientDirectionVertical) || (size.height == 0.0f && size.width == 0.0f)) {
        
        return nil;
    }
    
    NSDictionary *descriptors = @{ kUIImageColors : colors, kUIImageSize : [NSValue valueWithCGSize:size], kADDImageGradientDirection : @(direction), kUIImageResizableImage : @YES };
    
    UIImage *image = [self add_cachedImageWithDescriptors:descriptors];
    
    if (image) {
        
        return image;
        
    }
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    
    UIEdgeInsets insets = UIEdgeInsetsZero;
    
    if (direction == ADDImageGradientDirectionVertical) {
        imageSize.height = size.height;
        insets           = UIEdgeInsetsMake(0.0f, 1.0f, 0.0f, 1.0f);
    } else if (direction == ADDImageGradientDirectionHorizontal) {
        imageSize.width = size.width;
        insets          = UIEdgeInsetsMake(1.0f, 0.0f, 1.0f, 0.0f);
    } else {
        imageSize.width  = size.width;
        imageSize.height = size.height;
        insets           = UIEdgeInsetsMake(1.0f, 1.0f, 1.0f, 1.0f);
    }
    
    return [[self add_imageWithGradient:colors size:imageSize direction:direction] resizableImageWithCapInsets:insets];
}

- (UIImage *)add_imageWithSize:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality {
    BOOL drawTransposed;
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            drawTransposed = YES;
            break;
            
        default:
            drawTransposed = NO;
    }
    
    return [self add_resizedImage:newSize transform:[self add_transformForOrientation:newSize] drawTransposed:drawTransposed interpolationQuality:quality];
}

- (UIImage *)add_resizedImage:(CGSize)newSize transform:(CGAffineTransform)transform drawTransposed:(BOOL)transpose interpolationQuality:(CGInterpolationQuality)quality {
    CGRect newRect        = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectMake(0, 0, newRect.size.height, newRect.size.width);
    CGImageRef imageRef   = self.CGImage;
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL, newRect.size.width, newRect.size.height, CGImageGetBitsPerComponent(imageRef), 0, CGImageGetColorSpace(imageRef), CGImageGetBitmapInfo(imageRef));
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage      = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}

// Returns an affine transform that takes into account the image orientation when drawing a scaled image
- (CGAffineTransform)add_transformForOrientation:(CGSize)newSize {
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:          // EXIF = 3
        case UIImageOrientationDownMirrored:  // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:          // EXIF = 6
        case UIImageOrientationLeftMirrored:  // EXIF = 5
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:          // EXIF = 8
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:    // EXIF = 2
        case UIImageOrientationDownMirrored:  // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:   // EXIF = 5
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, newSize.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    return transform;
}

- (UIImage *)add_imageByFixingOrientation {
    // No-op if the orientation is already correct
    if (self.imageOrientation == UIImageOrientationUp)
        
        return self;
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
            
        default:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height, CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img     = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    
    return img;
}

- (UIImage *)imageWithOverlayColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
    
    if (&UIGraphicsBeginImageContextWithOptions) {
        CGFloat imageScale = 1.0f;
        
        if ([self respondsToSelector:@selector(scale)])  // The scale property is new with iOS4.
            imageScale = self.scale;
        UIGraphicsBeginImageContextWithOptions(self.size, NO, imageScale);
    } else {
        UIGraphicsBeginImageContext(self.size);
    }
    
    [self drawInRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)pbResizedImageWithWidth:(CGFloat)newWidth andTiledAreaFrom:(CGFloat)from1 to:(CGFloat)to1 andFrom:(CGFloat)from2 to:(CGFloat)to2 {
    NSAssert(self.size.width < newWidth, @"Cannot scale NewWidth %f > self.size.width %f", newWidth, self.size.width);
    CGFloat originalWidth  = self.size.width;
    CGFloat tiledAreaWidth = (newWidth - originalWidth) / 2;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(originalWidth + tiledAreaWidth, self.size.height), NO, self.scale);
    UIImage *firstResizable = [self resizableImageWithCapInsets:UIEdgeInsetsMake(30, from1, 0, originalWidth - to1) resizingMode:UIImageResizingModeTile];
    [firstResizable drawInRect:CGRectMake(0, 0, originalWidth + tiledAreaWidth, self.size.height)];
    UIImage *leftPart = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, self.size.height), NO, self.scale);
    UIImage *secondResizable = [leftPart resizableImageWithCapInsets:UIEdgeInsetsMake(30, from2 + tiledAreaWidth, 0, originalWidth - to2) resizingMode:UIImageResizingModeTile];
    [secondResizable drawInRect:CGRectMake(0, 0, newWidth, self.size.height)];
    UIImage *fullImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return fullImage;
}

+ (UIImage *)imageWithRadialGradientWithSize:(CGSize)size centerColor:(UIColor *)centerColor outsideColor:(UIColor *)outsideColor centerPoint:(CGPoint)centerPoint radius:(float)radius {
    static const size_t kNumberOfColors = 2;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    
    // Extract RGB color components from parameters
    size_t numberOfColorComponents = CGColorGetNumberOfComponents([centerColor CGColor]);
    CGFloat *colorComponents       = (CGFloat *)calloc(numberOfColorComponents * 2, sizeof(CGFloat));
    
    if (colorComponents == NULL) {
        
        UIGraphicsEndImageContext();
        
        return (nil);
    }
    memcpy(colorComponents, CGColorGetComponents([centerColor CGColor]), numberOfColorComponents * sizeof(CGFloat));
    memcpy(colorComponents + numberOfColorComponents, CGColorGetComponents([outsideColor CGColor]), numberOfColorComponents * sizeof(CGFloat));
    
    // Create the gradient
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient     = CGGradientCreateWithColorComponents(colorSpace, colorComponents, NULL, kNumberOfColors);
    
    // Normalize the center and radius
    CGPoint normalizedCenterPoint = CGPointMake(centerPoint.x * size.width, centerPoint.y * size.height);
    float normalizedRadius        = MIN(size.width, size.height) * radius;
    
    // Draw it
    CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), gradient, normalizedCenterPoint, 0, normalizedCenterPoint, normalizedRadius, kCGGradientDrawsAfterEndLocation);
    
    // Grab it as an autoreleased image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Clean up
    if (colorComponents != NULL) free(colorComponents);
    CGColorSpaceRelease(colorSpace);
    CGGradientRelease(gradient);
    UIGraphicsEndImageContext();
    
    return (image);
}

- (NSInteger)imageDataSize {
    NSData *imageData  = UIImagePNGRepresentation(self);
    NSInteger dataSize = imageData.length / 1024;
    
    return dataSize;
}

+ (UIImage *)compressImage:(UIImage *)image limitedDataSize:(NSInteger)limitedDataSize withBlock:(CommonBlock)block {
    UIImage *compressedImage;
    
    NSMutableString *logString = [[NSMutableString alloc] init];
    
    [logString appendFormat:@"Original image size: %ldkB", (long)image.imageDataSize];
    [logString appendFormat:@"\nExpected image size: %ldkB", (long)limitedDataSize];
    
    if (image.imageDataSize < limitedDataSize) {
        [logString appendFormat:@"\nNo compression is needed"];
        compressedImage = image;
        block(compressedImage, nil);
    } else {
        float compressionRatio = (float)limitedDataSize / (float)image.imageDataSize;
        
        float actualHeight = image.size.height;
        float actualWidth  = image.size.width;
        
        float newHeight = actualHeight * sqrt(compressionRatio);
        float newWidth  = actualWidth * sqrt(compressionRatio);
        
        CGRect rect = CGRectMake(0.0, 0.0, newWidth, newHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img      = UIGraphicsGetImageFromCurrentImageContext();
        NSData *imageData = UIImagePNGRepresentation(img);
        UIGraphicsEndImageContext();
        
        compressedImage = [UIImage imageWithData:imageData];
        [logString appendFormat:@"\nCompressed image size: %ldkB", (long)compressedImage.imageDataSize];
        block(compressedImage, nil);
    }
    
    return compressedImage;
}

+ (NSData *)compressImageToNSData:(UIImage *)image limitedDataSize:(NSInteger)limitedDataSize {
    UIImage *compressedImage;
    
    NSMutableString *logString = [[NSMutableString alloc] init];
    
    [logString appendFormat:@"Original image size: %ldkB", (long)image.imageDataSize];
    [logString appendFormat:@"\nExpected image size: %ldkB", (long)limitedDataSize];
    
    if (image.imageDataSize < limitedDataSize) {
        [logString appendFormat:@"\nNo compression is needed"];
        compressedImage = image;
        
    } else {
        float compressionRatio = (float)limitedDataSize / (float)image.imageDataSize;
        
        float actualHeight = image.size.height;
        float actualWidth  = image.size.width;
        
        float newHeight = actualHeight * sqrt(compressionRatio);
        float newWidth  = actualWidth * sqrt(compressionRatio);
        
        CGRect rect = CGRectMake(0.0, 0.0, newWidth, newHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img      = UIGraphicsGetImageFromCurrentImageContext();
        NSData *imageData = UIImagePNGRepresentation(img);
        UIGraphicsEndImageContext();
        
        compressedImage = [UIImage imageWithData:imageData];
        [logString appendFormat:@"\nCompressed image size: %ldkB", (long)compressedImage.imageDataSize];
        
        if (compressedImage.imageDataSize < limitedDataSize) {
            [logString appendFormat:@"\ncompressedImage.imageDataSize<limitedDataSize"];
            [logString appendFormat:@"\nNo compression is needed"];
            compressedImage = image;
        }
    }
    
    DDLogDebug(@"%@", logString);
    
    return UIImageJPEGRepresentation(compressedImage, 1);
}

+ (id<SDWebImageOperation>)loadWithURL:(NSURL *)url completion:(CommonBlock)completion {
    SDWebImageManager *manager        = [SDWebImageManager sharedManager];
    id<SDWebImageOperation> opertaion = [manager downloadImageWithURL:url
                                                              options:SDWebImageHighPriority
                                                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                 
                                                             }
                                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                                completion(image, error);
                                                            }];
    
    return opertaion;
}
//+ (AFHTTPRequestOperation*)operationWithURL:(NSURL *)url completion:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success{
//    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:url]];
//    requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
//    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        success(operation,responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        success(operation,nil);
//    }];
//    return requestOperation;
//}

+ (BOOL)imageExistForURL:(NSURL *)url {
    BOOL disk = [[SDImageCache sharedImageCache] diskImageExistsWithKey:[[SDWebImageManager sharedManager] cacheKeyForURL:url]];
    BOOL ram  = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[[SDWebImageManager sharedManager] cacheKeyForURL:url]];
    
    return disk || ram;
    
}

@end

#pragma mark - Categories

@implementation NSValue (NSValueADDCornerInsetExtensions)

+ (NSValue *)valueWithADDCornerInset:(ADDCornerInset)cornerInset {
    CGRect rect = CGRectMake(cornerInset.topLeft, cornerInset.topRight, cornerInset.bottomLeft, cornerInset.bottomRight);
    
    return [NSValue valueWithCGRect:rect];
    
    //    ADDCornerInset inset = cornerInset;
    //    return [[NSValue alloc] initWithBytes:&inset objCType:@encode(struct __ADDCornerInset)];
}

- (ADDCornerInset)ADDCornerInsetValue {
    CGRect rect = [self CGRectValue];
    
    return ADDCornerInsetMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    //    ADDCornerInset cornerInset;
    //    [self getValue:&cornerInset];
    //    return cornerInset;
}

@end
