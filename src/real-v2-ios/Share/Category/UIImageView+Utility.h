//
//  UIImageView+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
typedef enum {
  UIImageViewBorderTypeNone = 0,
  UIImageViewBorderTypeFollowing,
  UIImageViewBorderTypeFollowingSmall,
  UIImageViewBorderTypeRealCS,
  UIImageViewBorderTypeRealCSSmall

} UIImageViewBorderType;

@interface UIImageView (Utility)
- (void)loadImageURL:(NSURL *)url withIndicator:(BOOL)showIndicator;
- (void)loadImageURL:(NSURL *)url withIndicator:(BOOL)showIndicator withCompletion:(SDWebImageCompletionBlock)completionBlock;
- (void)setShowActivityIndicatorView:(BOOL)show;
- (void)setIndicatorStyle:(UIActivityIndicatorViewStyle)style;
- (void)cancelImageOperation;
@property (nonatomic, assign) UIImageViewBorderType borderType;

@end
