//
//  UIImageViewUtility.m
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <PureLayout/PureLayout.h>
#import <objc/runtime.h>
#import "UIImage+Utility.h"
#import "UIImageView+Utility.h"
static CGFloat kImageBorderWidthSmall  = 1.5f;
static CGFloat kImageBorderWidthNormal = 2.5f;
static char TAG_ACTIVITY_INDICATOR;
static char TAG_ACTIVITY_STYLE;
static char TAG_ACTIVITY_SHOW;
static char TAG_IMAGE_VIEW_BORDER_TYPE;

@implementation UIImageView (Utility)

- (void)loadImageURL:(NSURL *)url withIndicator:(BOOL)showIndicator {
  __weak typeof(self) weakSelf = self;
  [self loadImageURL:url
       withIndicator:showIndicator
      withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (cacheType == SDImageCacheTypeNone && image) {
          weakSelf.alpha = 0.0;
          [UIView animateWithDuration:0.3
                           animations:^{
                             weakSelf.alpha = 1.0;
                           }];
        }
      }];
}

- (void)loadImageURL:(NSURL *)url withIndicator:(BOOL)showIndicator withCompletion:(SDWebImageCompletionBlock)completionBlock {
  __weak typeof(self) weakSelf = self;
  [self setShowActivityIndicatorView:showIndicator];
  [self setIndicatorStyle:UIActivityIndicatorViewStyleGray];
  self.contentMode = UIViewContentModeScaleAspectFill;
    
  if ([self showActivityIndicatorView]) {
    [self addActivityIndicator];
  }
  [self sd_setImageWithURL:url
          placeholderImage:[UIImage imageWithColor:[UIColor lightGrayColor]]
                   options:0
                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                   completionBlock(image, error, cacheType, imageURL);
                   [weakSelf removeActivityIndicator];
                 }];
}

- (void)cancelImageOperation {
  [self sd_cancelCurrentImageLoad];
}

#pragma mark -

- (UIImageViewBorderType)borderType {
  id object = objc_getAssociatedObject(self, &TAG_IMAGE_VIEW_BORDER_TYPE);
    
  if (object) {
    return (UIImageViewBorderType)[object integerValue];
  } else {
    return 0;
  }
}

- (void)setBorderType:(UIImageViewBorderType)borderType {
  if (self.borderType != borderType) {
    objc_setAssociatedObject(self, &TAG_IMAGE_VIEW_BORDER_TYPE, [NSNumber numberWithInt:borderType], OBJC_ASSOCIATION_RETAIN);
      
    switch (self.borderType) {
      case UIImageViewBorderTypeNone:
        self.layer.borderWidth = 0;
        self.layer.borderColor = [UIColor clearColor].CGColor;
        break;
      case UIImageViewBorderTypeFollowing:
        self.layer.borderColor = RealBlueColor.CGColor;
        self.layer.borderWidth = kImageBorderWidthNormal;
        break;
      case UIImageViewBorderTypeRealCS:
        self.layer.borderColor = RealGreenColor.CGColor;
        self.layer.borderWidth = kImageBorderWidthNormal;
        break;
      case UIImageViewBorderTypeFollowingSmall:
        self.layer.borderColor = RealBlueColor.CGColor;
        self.layer.borderWidth = kImageBorderWidthSmall;
        break;
      case UIImageViewBorderTypeRealCSSmall:
        self.layer.borderColor = RealGreenColor.CGColor;
        self.layer.borderWidth = kImageBorderWidthSmall;
        break;
      default:
        break;
    }
  }
}

- (UIActivityIndicatorView *)activityIndicator {
  return (UIActivityIndicatorView *)objc_getAssociatedObject(self, &TAG_ACTIVITY_INDICATOR);
}

- (void)setActivityIndicator:(UIActivityIndicatorView *)activityIndicator {
  objc_setAssociatedObject(self, &TAG_ACTIVITY_INDICATOR, activityIndicator, OBJC_ASSOCIATION_RETAIN);
}

- (void)setShowActivityIndicatorView:(BOOL)show {
  objc_setAssociatedObject(self, &TAG_ACTIVITY_SHOW, [NSNumber numberWithBool:show], OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)showActivityIndicatorView {
  return [objc_getAssociatedObject(self, &TAG_ACTIVITY_SHOW) boolValue];
}

- (void)setIndicatorStyle:(UIActivityIndicatorViewStyle)style {
  objc_setAssociatedObject(self, &TAG_ACTIVITY_STYLE, [NSNumber numberWithInt:style], OBJC_ASSOCIATION_RETAIN);
}

- (int)getIndicatorStyle {
  return [objc_getAssociatedObject(self, &TAG_ACTIVITY_STYLE) intValue];
}

- (void)addActivityIndicator {
  if (!self.activityIndicator) {
    self.activityIndicator                                           = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:[self getIndicatorStyle]];
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;

    dispatch_main_async_safe(^{
      [self addSubview:self.activityIndicator];
      [self.activityIndicator autoCenterInSuperview];
    });
  }

  dispatch_main_async_safe(^{
    [self.activityIndicator startAnimating];
  });
}

- (void)removeActivityIndicator {
  if (self.activityIndicator) {
    [self.activityIndicator removeFromSuperview];
    self.activityIndicator = nil;
  }
}

- (UIImageView *)setImageBorderBlue:(UIImageView *)personalImageView {
  personalImageView.layer.borderWidth = 1.5f;
  personalImageView.layer.borderColor = [UIColor colorWithRed:(0 / 255.0) green:(153 / 255.0) blue:(255 / 255.0) alpha:1.0].CGColor;

  return personalImageView;
}

- (UIImageView *)setImageBorderNone:(UIImageView *)personalImageView {
  personalImageView.layer.borderWidth = 0;

  return personalImageView;
}

@end
