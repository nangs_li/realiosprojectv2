//
//  UIView+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
enum {
  RelativeDirectionLeft = 0,
  RelativeDirectionRight,
  RelativeDirectionTop,
  RelativeDirectionBottom,
};
typedef NSUInteger RelativeDirection;

enum {
  ViewAlignmentCenter = 0,
  ViewAlignmentLeft,
  ViewAlignmentRight,
  ViewAlignmentTop,
  ViewAlignmentBottom,
  ViewAlignmentHorizontalCenter,
  ViewAlignmentVerticalCenter,
};
typedef NSUInteger ViewAlignment;

@interface UIView (Utility)

- (void)moveViewTo:(UIView *)targetView direction:(RelativeDirection)direction padding:(int)padding;
- (void)setViewAlignmentInSuperView:(ViewAlignment)aligment padding:(int)padding;
- (void)align:(UIView *)targetView direction:(ViewAlignment)aligment padding:(int)padding;
- (void)addSlidingBlinkEffect:(CGFloat)gradientWidth transparency:(CGFloat)transparency duration:(CGFloat)duration repeatCount:(CGFloat)repeat;
- (void)addFlashingEffect:(UIColor *)flashingColor duration:(CGFloat)duration;
- (void)addRadialGradient:(CGFloat)duration;

@end
