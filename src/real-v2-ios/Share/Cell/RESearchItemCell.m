//
//  RESearchItemCell.m
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RESearchItemCell.h"

@implementation RESearchItemCell

+ (UINib *)nib {
    return [UINib nibWithNibName:@"RESearchItemCell" bundle:nil];
}

- (void)configureWithTitle:(NSString *)title {
    self.titleLabel.text = title;
}

@end
