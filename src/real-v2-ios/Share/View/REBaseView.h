//
//  REBaseView.h
//  real-v2-ios
//
//  Created by Li Ken on 5/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REAgentProfile.h"
#import "REAgentListing.h"

@interface REBaseView : UIView

@end
