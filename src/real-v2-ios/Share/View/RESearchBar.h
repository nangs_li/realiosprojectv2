//
//  RESearchBar.h
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RETitleBar.h"

#import "RESearchItemCell.h"

@class RESearchBar;

@protocol RESearchBarDelegate <NSObject>

- (void)searchBar:(RESearchBar *)searchBar didSearchKeyword:(NSString *)keyword;
- (void)searchBar:(RESearchBar *)searchBar didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)searchBarDidCancelSearching:(RESearchBar *)searchBar;

@end

@protocol RESearchBarDataSource <NSObject>

- (NSInteger)numberOfItemInSearchBar:(RESearchBar *)searchBar;
- (RESearchItemCell *)searchBar:(RESearchBar *)searchBar itemCell:(RESearchItemCell *)itemCell atIndexPath:(NSIndexPath *)indexPath;

@end

@interface RESearchBar : RETitleBar <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, weak) id<RESearchBarDelegate> delegate;
@property (nonatomic, weak) id<RESearchBarDataSource> dataSource;

- (BOOL)isSearching;
- (void)reloadSearchData;
- (void)startSearching;
- (void)endSearching;
- (void)updateResultCountString:(NSString *)countString;
- (void)updateKeyword:(NSString *)keyword;

- (id)initWithParent:(UIView *)parent;

@end
