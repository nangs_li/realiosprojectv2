//
//  RESearchBar.m
//  real-v2-ios
//
//  Created by Alex Hung on 14/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RESearchBar.h"
#import "RESearchItemCell.h"
#import <PureLayout/PureLayout.h>

@interface RESearchBar ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UITableView *searchTableView;
@property (nonatomic, strong) UILabel *resultCountLabel;

@property (nonatomic, strong) NSTimer *searchInputTimer;
@property (nonatomic, strong) NSMutableArray *searchItems;

@property (nonatomic, strong) NSLayoutConstraint *textFieldSuperViewTrailingConstraint;
@property (nonatomic, strong) NSLayoutConstraint *cancelButtonSuperViewTrailingConstraint;
@property (nonatomic, strong) UIView *parentView;

@end

@implementation RESearchBar

- (id)initWithParent:(UIView *)parent {
    self = [super init];
    
    if (self) {
        self.parentView = parent;
        [self initViews];
    }
    
    return self;
}

- (id)init {
    self = [super init];
    
    if (self) {
        self.parentView = self;
        [self initViews];
    }
    
    return self;
}

- (void)removeFromSuperview {
    [self.searchTableView removeFromSuperview];
    [super removeFromSuperview];
}

- (void)didMoveToSuperview {
    [self.searchTableView removeFromSuperview];
    
    if (self.parentView.superview) {
        self.searchTableView = [UITableView newAutoLayoutView];
        self.searchTableView.tableFooterView = [UIView new];
        self.searchTableView.backgroundColor = [UIColor REDimBackgroundColor];
        [self.searchTableView registerNib:[RESearchItemCell nib] forCellReuseIdentifier:@"RESearchItemCell"];
        self.searchTableView.delegate = self;
        self.searchTableView.dataSource = self;
        self.searchTableView.estimatedRowHeight = 44;
        self.searchTableView.rowHeight = UITableViewAutomaticDimension;
        
        [self.parentView.superview insertSubview:self.searchTableView belowSubview:self.parentView];
        
        [self.searchTableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.parentView];
        [self.searchTableView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [self.searchTableView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
        [self.searchTableView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
        
        
        [self showSearchTableView:NO animated:NO];
    }
}

- (void)initViews {
    
    if (!self.textField) {
        
        self.textField = [UITextField newAutoLayoutView];
        self.textField.layer.borderColor = [UIColor whiteColor].CGColor;
        self.textField.layer.borderWidth = 1;
        self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search places" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        self.textField.textColor = [UIColor whiteColor];
        
        UIImageView *searchIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 8.0f, 18.0f)];
        self.textField.leftView = searchIconImageView;
        self.textField.leftViewMode = UITextFieldViewModeAlways;
        
        self.resultCountLabel = [[UILabel alloc]init];
        self.resultCountLabel.textColor = [UIColor whiteColor];
        self.resultCountLabel.font = [UIFont systemFontOfSize:14];
        self.resultCountLabel.text = @"";
        self.textField.rightView = self.resultCountLabel;
        self.textField.rightViewMode = UITextFieldViewModeUnlessEditing;
        
        self.textField.delegate = self;
        [self addSubview:self.textField];
        
    }
    
    if (!self.cancelButton) {
        
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(cancelButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelButton];
        
    }
    
    [self setupViews];
}

- (void)setupViews {
    [self setupSearchConstraint];
}

- (void)setupSearchConstraint {
    [self.textField autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:16];
    [self.textField autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:16];
    [self.textField autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:16];
    self.textFieldSuperViewTrailingConstraint = [self.textField autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:16];
    [self.textField autoSetDimension:ALDimensionHeight toSize:34];
    
    [self.cancelButton autoPinEdge:ALEdgeLeading toEdge:ALEdgeTrailing ofView:self.textField withOffset:16];
    self.cancelButtonSuperViewTrailingConstraint =  [self.cancelButton autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:16];
    [self.cancelButton autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.textField];
    
    [NSLayoutConstraint autoSetPriority:ALLayoutPriorityRequired forConstraints:^{
        [self.cancelButton autoSetContentHuggingPriorityForAxis:ALAxisHorizontal];
        [self.cancelButton autoSetContentCompressionResistancePriorityForAxis:ALAxisHorizontal];
    }];
    
    self.cancelButtonSuperViewTrailingConstraint.active = NO;
}

- (void)showSearchTableView:(BOOL)show animated:(BOOL)animated {
    
    if (animated) {
        self.searchTableView.alpha = !show;
        self.searchTableView.hidden = NO;
    }
    
    void (^animation) () = ^ {
        self.searchTableView.alpha = show;
    };
    
    void (^completion) (BOOL finished) = ^(BOOL finished) {
        self.searchTableView.hidden = !show;
    };
    
    
    if (!animated) {
        animation();
        completion(YES);
    } else {
        [UIView animateWithDuration:kAnimationDuration animations:animation completion:completion];
    }
    
}

- (void)reset {
    self.textField.text = nil;
    self.searchItems = [[NSMutableArray alloc]init];
    [self.searchTableView reloadData];
}

- (void)search {
    //didChangeTextField --> pendingToSearch --> search --> searchBar:didSearchKeyword
    
    if ([self.delegate respondsToSelector:@selector(searchBar:didSearchKeyword:)]) {
        [self.delegate searchBar:self didSearchKeyword:[self.textField.text trim]];
    }
    
}

- (BOOL)isSearching {
    return !self.searchTableView.hidden;
}

- (void)reloadSearchData {
    [self.searchTableView reloadData];
}

- (void)startSearching {
    [self.textField becomeFirstResponder];
}

- (void)endSearching {
    [self.textField resignFirstResponder];
}

- (void)updateResultCountString:(NSString *)countString {
    self.resultCountLabel.text = countString;
    [self.resultCountLabel sizeToFit];
    CGRect labelFrame = self.resultCountLabel.frame;
    labelFrame.origin.x = 8;
    labelFrame.size.height = self.textField.frame.size.height;
    labelFrame.size.width += 8;
    self.resultCountLabel.frame = labelFrame;
}

- (void)updateKeyword:(NSString *)keyword {
    self.textField.text = keyword;
}

- (void)pendingToSearch {
    
    if (self.searchInputTimer) {
        [self.searchInputTimer invalidate];
    }
    
    self.searchInputTimer = [NSTimer scheduledTimerWithTimeInterval:kSearchBarInputDelayDuration target:self selector:@selector(search) userInfo:nil repeats:NO];
}

- (void)becomeActive:(BOOL)active {
    self.cancelButton.alpha = !active;
    self.cancelButton.hidden = NO;
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.textFieldSuperViewTrailingConstraint.active = !active;
        self.cancelButtonSuperViewTrailingConstraint.active = active;
        self.cancelButton.alpha = active;
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.cancelButton.hidden = !active;
    }];
    
}

#pragma mark - IBAction

- (IBAction)cancelButtonDidPress:(id)sender {
    [self.textField resignFirstResponder];
    [self reset];
    
    if ([self.delegate respondsToSelector:@selector(searchBarDidCancelSearching:)]) {
        [self.delegate searchBarDidCancelSearching:self];
    }
}

#pragma mark - UITableViewDelegate;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.delegate respondsToSelector:@selector(searchBar:didSelectItemAtIndexPath:)]) {
        [self.delegate searchBar:self didSelectItemAtIndexPath:indexPath];
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.dataSource respondsToSelector:@selector(numberOfItemInSearchBar:)]) {
        
        return [self.dataSource numberOfItemInSearchBar:self];
        
    } else {
        
        NSAssert(false, @"Please implement numberOfItemInSearchBar:");
        
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RESearchItemCell *itemCell = [tableView dequeueReusableCellWithIdentifier:@"RESearchItemCell"];
    
    if ([self.dataSource respondsToSelector:@selector(searchBar:itemCell:atIndexPath:)]) {
        
        return [self.dataSource searchBar:self itemCell:itemCell atIndexPath:indexPath];
        
    } else {
        
        NSAssert(false, @"Please implement searchBar:cellForIndexPathAtIndexPath:");
        
        return nil;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self pendingToSearch];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self becomeActive:YES];
    [self showSearchTableView:YES animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self becomeActive:NO];
    [self showSearchTableView:NO animated:YES];
}

@end
