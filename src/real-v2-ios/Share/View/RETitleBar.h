//
//  RETitleBar.h
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RETitleBar : UIView

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLabel;

+ (RETitleBar *)titleBarWithTitle:(NSString *)title;

@end
