//
//  RETitleBar.m
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RETitleBar.h"
#import "UIColor+REColor.h"
#import <PureLayout/PureLayout.h>

@implementation RETitleBar

+ (RETitleBar *)titleBarWithTitle:(NSString *)title {
    RETitleBar *titleBar = [[RETitleBar alloc]init];
    titleBar.backgroundColor = [UIColor RETopBarBlueColor];
    
    titleBar.contentView = [[UIView alloc]init];
    titleBar.contentView.backgroundColor = [UIColor clearColor];
    [titleBar addSubview:titleBar.contentView];
    [titleBar.contentView autoPinEdgesToSuperviewEdges];
    
    titleBar.titleLabel = [[UILabel alloc]init];
    titleBar.titleLabel.textColor = [UIColor whiteColor];
    
    titleBar.titleLabel.backgroundColor = [UIColor clearColor];
    titleBar.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBar.titleLabel.text = title;
    [titleBar.contentView addSubview:titleBar.titleLabel];
    
    [titleBar.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [titleBar.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [titleBar.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [titleBar.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20];
    
    return titleBar;
}

@end
