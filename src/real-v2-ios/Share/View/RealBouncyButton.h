//
//  RealBouncyButton.h
//  productionreal2
//
//  Created by Derek Cheung on 3/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RealBouncyButton;

@protocol RealBouncyButtonDelegate <NSObject>

@optional

- (void)buttonDidPressed:(RealBouncyButton *)sender;

@end

@interface RealBouncyButton : UIButton

@property (nonatomic, strong) id<RealBouncyButtonDelegate> delegate;

@end
