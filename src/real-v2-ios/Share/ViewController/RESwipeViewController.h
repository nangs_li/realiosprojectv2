//
//  RESwipeViewController.h
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MMDrawerController.h"

@interface RESwipeViewController : MMDrawerController

+ (RESwipeViewController *)swipeViewControllerWithLeftViewController:(REViewController *)leftVC centerViewController:(REViewController *)centerVC rightViewController:(REViewController *)rightVC;

@end
