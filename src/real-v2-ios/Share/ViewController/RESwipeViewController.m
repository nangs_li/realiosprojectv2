//
//  RESwipeViewController.m
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RESwipeViewController.h"
#import "REViewController.h"
#import "RETitleBar.h"

#import <PureLayout/PureLayout.h>
#import "REConstant.h"

@interface RESwipeViewController ()

@end

@implementation RESwipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews {
    [self setupTitleBar];
}

- (void)setupTitleBar {
    self.titleBar = [[RETitleBar alloc]init];
    [self.view addSubview:self.titleBar];
    self.titleBar.backgroundColor = [UIColor RETopBarBlueColor];
    [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [self.titleBar autoSetDimension:ALDimensionHeight toSize:100];
}

+ (RESwipeViewController *)swipeViewControllerWithLeftViewController:(REViewController *)leftVC centerViewController:(REViewController *)centerVC rightViewController:(REViewController *)rightVC {
    RESwipeViewController *swipeViewController = [[RESwipeViewController alloc]initWithCenterViewController:centerVC leftDrawerViewController:leftVC rightDrawerViewController:rightVC];
    [swipeViewController.view bringSubviewToFront:swipeViewController.titleBar];
    
    leftVC.customTitleBarContainer = swipeViewController.titleBar;
    leftVC.titleBar.alpha = 0;
    
    centerVC.customTitleBarContainer = swipeViewController.titleBar;
    centerVC.titleBar.alpha = 1;
    
    rightVC.customTitleBarContainer = swipeViewController.titleBar;
    rightVC.titleBar.alpha = 0;
    
    //TODO: Remove after demo
    leftVC.titleBar.titleLabel.text = @"Left";
    leftVC.contentView.backgroundColor = [UIColor brownColor];
    centerVC.titleBar.titleLabel.text = @"Center";
    centerVC.contentView.backgroundColor = [UIColor grayColor];
    rightVC.titleBar.titleLabel.text = @"Right";
    rightVC.contentView.backgroundColor = [UIColor blackColor];
    
    [swipeViewController setShowsShadow:YES];
    [swipeViewController setRestorationIdentifier:@"MMDrawer"];
    [swipeViewController setMaximumRightDrawerWidth:200.0];
    [swipeViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [swipeViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    CGRect screenRect = [RealUtility screenBounds];
    CGFloat screenWidth = screenRect.size.width;
    [swipeViewController setMaximumLeftDrawerWidth:screenWidth];
    [swipeViewController setMaximumRightDrawerWidth:screenWidth];
    CGFloat parallaxFactor = 4.0f;
    CGFloat alphaFactor = 3.0;
    MMDrawerControllerDrawerVisualStateBlock visualStateBlock = ^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
        //       NSParameterAssert(parallaxFactor >= 1.0);
        CATransform3D transform = CATransform3DIdentity;
        UIViewController *sideDrawerViewController;
        CGFloat navAlpha = percentVisible * alphaFactor;
        
        if (navAlpha < 0) {
            navAlpha = 0;
        } else if (navAlpha > 1) {
            navAlpha = 1;
        }
        
        UIViewController *centerViewController = drawerController.centerViewController;
        
        if ([centerViewController isKindOfClass:[REViewController class]]) {
            REViewController *centerBaseViewController = (REViewController *)centerViewController;
            centerBaseViewController.titleBar.alpha = 1 - navAlpha;
            CGFloat opactiy = navAlpha * 2.0;
            
            if (opactiy < 0) {
                opactiy = 0;
            } else if (opactiy > 0.8) {
                opactiy = 0.8;
            }
            
            centerBaseViewController.spotlightLayer.opacity = opactiy;
        }
        
        if (drawerSide == MMDrawerSideLeft) {
            sideDrawerViewController = drawerController.leftDrawerViewController;
            
            if ([sideDrawerViewController isKindOfClass:[REViewController class]]) {
                REViewController *leftBaseViewController = (REViewController *)sideDrawerViewController;
                leftBaseViewController.titleBar.alpha = navAlpha;
            }
            CGFloat distance = MAX(drawerController.maximumLeftDrawerWidth, drawerController.visibleLeftDrawerWidth) - 1;
            
            if (percentVisible <= 1.0) {
                CGFloat translation = (-distance) / parallaxFactor + (distance * percentVisible / parallaxFactor);
                transform = CATransform3DMakeTranslation(translation, 0.0, 0.0);
            } else {
                transform = CATransform3DMakeScale(percentVisible, 1.0, 1.0);
                transform = CATransform3DTranslate(transform, drawerController.maximumLeftDrawerWidth * (percentVisible - 1.0) / 2, 0.0, 0.0);
            }
        } else if (drawerSide == MMDrawerSideRight) {
            sideDrawerViewController = drawerController.rightDrawerViewController;
            
            if ([sideDrawerViewController isKindOfClass:[REViewController class]]) {
                REViewController *rightBaseViewController = (REViewController *)sideDrawerViewController;
                rightBaseViewController.titleBar.alpha = navAlpha;
            }
            
            CGFloat distance = MAX(drawerController.maximumRightDrawerWidth, drawerController.visibleRightDrawerWidth) - 1;
            
            if (percentVisible <= 1.0) {
                CGFloat translation = (distance) / parallaxFactor - (distance * percentVisible) / parallaxFactor;
                transform = CATransform3DMakeTranslation(translation, 0.0, 0.0);
            } else {
                transform = CATransform3DMakeScale(percentVisible, 1.0, 1.0);
                transform = CATransform3DTranslate(transform, drawerController.maximumRightDrawerWidth * (percentVisible - 1.0) / 2, 0.0, 0.0);
            }
        }
        [sideDrawerViewController.view.layer setTransform:transform];
    };
    [swipeViewController setDrawerVisualStateBlock:visualStateBlock];
    
    return swipeViewController;
}

@end
