//
//  RETabBarController.h
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REConstant.h"

@class RETabBar;

@interface RETabBarController : UIColor

@property (nonatomic, strong) RETabBar *tabBar;
@property (nonatomic, strong) NSArray<UIViewController *> *viewControllers;

- (void)selectAtIndex:(RETabIndex)index;
- (void)updateBubbleCount:(NSInteger)count atIndex:(RETabIndex)tabIndex;

- (void)showTabBar;
- (void)hideTabBar;

@end
