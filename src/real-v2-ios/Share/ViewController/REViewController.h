//
//  REViewController.h
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class RETitleBar;

@interface REViewController : UIViewController

@property (nonatomic, strong) RETitleBar *titleBar;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *customTitleBarContainer;

@property (nonatomic, strong) CAShapeLayer *spotlightLayer;

- (void)setupViews;
- (void)setupTitleBar;
- (void)setupContentView;

@end
