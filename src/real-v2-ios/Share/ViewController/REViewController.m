//
//  REViewController.m
//  real-v2-ios
//
//  Created by Alex Hung on 3/8/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REViewController.h"
#import "RETitleBar.h"
#import <PureLayout/PureLayout.h>
#import "MMDrawerController.h"

@interface REViewController () <MMDrawerTouchDelegate>

@end

@implementation REViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self setupViews];
    }
    
    return self;
}

- (void)setupViews {
    [self setupTitleBar];
    [self setupContentView];
}

- (void)setupTitleBar {
    [self.titleBar removeFromSuperview];
    UIView *containerView = self.customTitleBarContainer ? self.customTitleBarContainer : self.view;
    self.titleBar = [RETitleBar titleBarWithTitle:[NSString stringWithFormat:@"%d", (int)[self hash]]];
    [containerView addSubview:self.titleBar];
    
    if (self.customTitleBarContainer) {
        self.titleBar.backgroundColor = [UIColor clearColor];
        [self.titleBar autoPinEdgesToSuperviewEdges];
    } else {
        [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeLeading];
        [self.titleBar autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
        [self.titleBar autoSetDimension:ALDimensionHeight toSize:64];
    }
}

- (void)setupContentView {
    [self.contentView removeFromSuperview];
    self.contentView = [[UIView alloc]init];
    [self.view addSubview:self.contentView];
    
    if (self.customTitleBarContainer) {
        [self.contentView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.titleBar];
    } else {
        [self.contentView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    }
    
    [self.contentView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.contentView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [self.contentView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

- (void)setCustomTitleBarContainer:(UIView *)customTitleBarContainer {
    _customTitleBarContainer = customTitleBarContainer;
    [self setupTitleBar];
    [self setupContentView];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

@end
