//
//  main.m
//  real-v2-ios
//
//  Created by Derek Cheung on 16/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
